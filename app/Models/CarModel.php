<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property int $car_number
 * @property string $note
 */
class CarModel extends BaseModel
{
    protected $table = 'car';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['car_number', 'note', 'image_front', 'image_car', 'created_by'];
    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
            'car_number' =>[
                'rules'  => 'required|min_length[3]|max_length[255]|is_unique[car.car_number]',
                'errors' => [
                    'required' => 'Biển số không được để trống',
                    'min_length'=> 'Biển số có ít nhất 3 ký tự',
                    'max_length'=> 'Biển số có nhiều nhất 200 ký tự',
                    'is_unique'=> 'Biển số không được trùng lặp'
                ]
            ],
            'image_front' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh biển số không được để trống',
                ]
            ],
            'image_car' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh xe không được để trống',
                ]
            ],
        ];
    }
}