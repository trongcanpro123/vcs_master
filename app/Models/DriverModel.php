<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property string $title
 * @property string $slug
 * @property int $category_id
 * @property string $intro
 * @property string $content
 * @property string $image
 * @property string $material
 * @property string $guarantee
 * @property int $price
 * @property int $discount
 * @property int $is_lock
 */
class DriverModel extends BaseModel
{
    protected $table = 'driver';
    protected $primaryKey = 'id';
    
    protected $useSoftDeletes = false;
    protected $allowedFields = ['driver_name', 'phone_number', 'image', 'image_front', 'image_left', 'image_right','created_by'];
    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
            'driver_name' =>[
                'rules'  => 'required|min_length[3]|max_length[255]',
                'errors' => [
                    'required' => 'Biển số không được để trống',
                    'min_length'=> 'Biển số có ít nhất 3 ký tự',
                    'max_length'=> 'Biển số có nhiều nhất 200 ký tự',
                ]
            ],
            'phone_number' =>[
                'rules'  => 'required|is_unique[driver.phone_number]',
                'errors' => [
                    'required' => 'Số điện thoại không được để trống',
                    'is_unique'=> 'Số điện thoại không được trùng lặp'
                ]
            ],
            'image_front' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh chính diện không được để trống',
                ]
            ],
            'image_left' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh trái không được để trống',
                ]
            ],
            'image_right' =>[
                'rules'  => 'required',
                'errors' => [
                    'required' => 'Ảnh phải không được để trống',
                ]
            ],

        ];
    }

    

}