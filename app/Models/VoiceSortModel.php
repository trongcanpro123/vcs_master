<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;
use App\Models\VoiceOptionModel;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property int $car_number
 * @property string $note
 */
class VoiceSortModel extends BaseModel
{
    protected $table = 'voice_sort';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['tgbx_detail_id', 'pump_system_id', 'throad_id','pump_system_name','throad_name','item_name',
        'display_on','in_out_id','created_at','updated_at','alert_last_time','alert_count','alert_back','alert_order','file_alert','alert_message','alert_count_2'];
    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [

        ];
    }
    public static function generate_alert_message($throad_name, $in_out_id){
        $config = (new VoiceOptionModel())->first();
        $in_out = (new CarInOutHistoryModel())->find($in_out_id);
        $alert ='';
        if($in_out){
            $alert = 'Tài xế '.  $in_out->driver_name .', biển số '. $in_out->car_number .' chuẩn bị vào '.$throad_name. ' lấy hàng';
            if($config && $config->type_alert == 'so_thu_tu'){
                $alert = 'Số thứ tự '. $in_out->checkin_order .', chuẩn bị vào '.$throad_name. ' lấy hàng';
            }
        }


        return $alert;
    }

    public function delete_by_in_out_id($in_out_id){
        $this->db->query('delete from voice_sort where in_out_id = ? ',[$in_out_id]);

    }

    public function get_max_value_alert_order() {
        return $this->db->query('SELECT MAX(alert_order) as alert_order FROM  `voice_sort` ')->getResultArray();
    }

    public function get_list_voice_play_twice($throad_id) {
        $count_alert = 2;
        return $this->db->query('SELECT sort.*, in_out.car_number FROM `voice_sort` AS sort
                            INNER JOIN `car_in_out_history` AS in_out ON sort.in_out_id = in_out.id
                            WHERE 
                                file_alert is not null  and file_alert <> \'\'
                                and alert_count = ?
                                and in_out.checkout_time IS NULL 
                                and throad_id = ? 
                            ORDER BY sort.alert_order ASC limit 10 ', [$count_alert, $throad_id])->getResultArray();
    }
    // public function get_list_voice_waiting_on_throad($throad_id)
    // {
    //     $count_alert = 2;
    //     $config = (new VoiceOptionModel())->first();
    //     if($config) {
    //         $count_alert = $config->number_alert;
    //     }
    //     return $this->db->query('SELECT sort.*, in_out.car_number FROM `voice_sort` AS sort
    //                         INNER JOIN `car_in_out_history` AS in_out ON sort.in_out_id = in_out.id
    //                         WHERE 
    //                             file_alert is not null  and file_alert <> \'\'
    //                             and alert_count <= ?
    //                             and alert_count_2 < ?
    //                             and  in_out.checkout_time IS NULL 
    //                             and throad_id = ? 
    //                         ORDER BY sort.alert_order ASC limit 10 ', [$count_alert, $count_alert, $throad_id])->getResultArray();
    // }
    public function get_list_voice_waiting_on_throad()
    {
        // $count_alert = 2;
        // $config = (new VoiceOptionModel())->first();
        // if($config) {
        //     $count_alert = $config->number_alert;
        // }
        return $this->db->query('SELECT * FROM `voice_sort` where alert_count < 1 limit 1')->getResultArray();
    }
    

    public function get_voice_waiting_at_different_throad($in_out_id, $voice_id) {
        return $this->db->query('SELECT sort.*, in_out.car_number FROM `voice_sort` AS sort
                                INNER JOIN `car_in_out_history` AS in_out ON sort.in_out_id = in_out.id
                                WHERE 
                                    file_alert is not null  and file_alert <> \'\'
                                    and  in_out.checkout_time IS NULL 
                                    and  in_out_id = ? 
                                    and  sort.id < ?
                                ORDER BY sort.alert_order ASC limit 1 ', [$in_out_id, $voice_id])->getResult();
    }
}