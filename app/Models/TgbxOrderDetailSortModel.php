<?php


namespace App\Models;

use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;

class TgbxOrderDetailSortModel extends BaseModel
{
    protected $table = 'tgbx_order_detail_sort';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['tgbx_detail_id', 'pump_system_id', 'throad_id', 'pump_system_name', 'throad_name', 'item_name', 'display_on', 'in_out_id'];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $dateFormat = 'int';

    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
        ];
    }

    public function get_list_take_item_waiting_on_throad($throad_id, $limit = 4)
    {
//        return $this->db->query('SELECT sort.*, in_out.car_number FROM `tgbx_order_detail_sort` AS sort
  //                          INNER JOIN `car_in_out_history` AS in_out ON sort.in_out_id = in_out.id
    //                        WHERE in_out.checkout_time IS NULL  AND throad_id = ? ORDER BY sort.id ASC limit ?', [$throad_id, $limit])->getResultArray();
	return $this->db->query('SELECT sort.*, in_out.car_number FROM `tgbx_order_detail_sort` AS sort
                                INNER JOIN `car_in_out_history` AS in_out ON sort.in_out_id = in_out.id
                                INNER JOIN tgbx_order_detail AS tg ON sort.`in_out_id` = tg.`in_out_id` AND sort.`smo_id` = tg.`smo_id`
                           	WHERE in_out.checkout_time IS NULL  AND sort.throad_id = ? AND tg.`tdh_status` = 0
	                       	ORDER BY sort.id ASC limit ?', [$throad_id, $limit])->getResultArray();
    }

    // lay ds xe dang lay hang
    public function get_list_car_picking_item($throad_id) 
    {
        $tdh_status = 'dang_xuat';
        return $this->db->query('SELECT detail.*, in_out.car_number FROM `tgbx_order_detail` AS detail
                                 INNER JOIN `car_in_out_history` AS in_out ON detail.in_out_id = in_out.id
                                 WHERE in_out.checkout_time IS NULL AND throad_id = ? AND detail.tdh_status = ? ORDER BY detail.id DESC limit 1', [$throad_id, $tdh_status])->getRow();
    }

    public function get_list_take_item_waiting_alert($area_id, $display_on, $minute_alert = 5)
    {
        return $this->db->query('SELECT * FROM `car_in_out_history` AS in_out
                                 WHERE ticket = 1
                                 AND invite_take_item = 1
                                 AND checkout_time IS NULL
                                 AND area_id = ?
                                 AND id IN (SELECT in_out_id FROM `tgbx_order_detail`, `pump_throad` 
                                           WHERE   tgbx_order_detail.`throad_id` = pump_throad.id 
                                          AND pump_throad.`display_on`  = ?)
                                          AND TIMESTAMPDIFF(MINUTE,FROM_UNIXTIME(updated_at),NOW()) < ? ', [$area_id, $display_on, $minute_alert])->getResultArray();
    }

    public function delete_by_in_out_id($in_out_id){
        $this->db->query('delete from tgbx_order_detail_sort where in_out_id = ? ',[$in_out_id]);

    }
}
