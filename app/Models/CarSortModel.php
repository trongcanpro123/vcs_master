<?php


namespace App\Models;
//use App\Helpers\StringHelper;
//use App\Models\Interfaces\ContentInterface;

class CarSortModel extends BaseModel
{
    protected $table = 'car_sort';
    protected $primaryKey = 'id';
    protected $useSoftDeletes = false;
    protected $allowedFields = ['id_smo', 'process_status', 'sequence_time', 'update_time', 'car_in_out_id', 'display_on', 'throad_name', 'throad_id', 'pump_system_name', 'pump_system_id', 'trang_thai', 'ten_hang_hoa', 'ma_hang_hoa', 'so_lenh', 'ngay_xuat', 'area_id','phuong_tien'];
    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $dateFormat = 'int';

    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
        ];
    }
    public function truncate() {
        $this->db->query('TRUNCATE TABLE car_sort;');
    }
}
