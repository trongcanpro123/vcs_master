<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property string $title
 * @property string $slug
 * @property int $category_id
 * @property string $intro
 * @property string $content
 * @property string $image
 * @property string $material
 * @property string $guarantee
 * @property int $price
 * @property int $discount
 * @property int $is_lock
 */
class PumpSystemModel extends BaseModel
{
    protected $table = 'pump_system';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;

    protected $allowedFields = ['area_id','pump_name'];


    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    public function getRules(string $scenario = null): array
    {
        return [
            'pump_name' =>[
                'rules'  => 'required|min_length[3]|max_length[200]',
                'errors' => [
                    'required' => 'Tên giàn bơm không được để trống',
                    'min_length'=> 'Tên giàn bơm có ít nhất 3 ký tự',
                    'max_length'=> 'Tên giàn bơm có nhiều nhất 200 ký tự'
                ]
            ]
        ];
    }
    public function select_name_area()
    {
        $name_area_db = $this->db->query('SELECT `name` FROM area WHERE id = ?', [$this->area_id])->getRow();
        if ($name_area_db) {
            return $name_area_db->name;
        }
        return '';
    }


}
