<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property int $car_number
 * @property string $note
 */
class SmoOrderModel extends BaseModel
{
    protected $table = 'smo_order';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['in_out_history_id', 'po_code', 'doc_date', 'oic_pbatch', 'oic_ptrip',
        'vehicle_code','do_sap', 'status'];
    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
        ];
    }

    public function delete_all_qr_code_info($in_out_history_id){
        $this->db->query('delete from smo_order_detail where in_out_history_id = ?',$in_out_history_id);
        $this->db->query('delete from smo_order where in_out_history_id = ?',$in_out_history_id);
        return true;

    }

    public function delete_smo_info($in_out_id,$qr_code){
        $this->db->query('delete from smo_order_detail where in_out_history_id = ? and do_sap = ? ',[$in_out_id,$qr_code]);
        $this->db->query('delete from smo_order where in_out_history_id = ? and do_sap =? ',[$in_out_id,$qr_code]);

    }

    public function check_exits_smo ($in_out_id,$qr_code){
        $query = 'select * from car_in_out_history where id <> ? and id in (select in_out_history_id from smo_order where  do_sap =?) and checkin_order >= 0 ';
        $exist = $this->db->query($query,[$in_out_id,$qr_code])->getRow();
        if($exist){
            return true;
        }
        return false;
    }
}