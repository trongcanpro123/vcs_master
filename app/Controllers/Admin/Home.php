<?php

namespace App\Controllers\Admin;


use App\Controllers\BaseController;
use App\Models\AdministratorModel;

class Home extends BaseController
{
    public function index()
    {
//        $this->layout = 'empty';
        $identity = AdministratorModel::findIdentity();
        if($identity->type_user == 'lanh_dao'){

            return $this->response->redirect(route_to('ld_home'));
        }
        if($identity->type_user == 'lanh_dao_kho'){
            return $this->response->redirect(route_to('ld_home'));
        }
        if($identity->type_user == 'nhan_vien'){
            return $this->response->redirect(route_to('nv_order_take_ticket'));
        }
        if($identity->type_user == 'bao_ve'){
            return $this->response->redirect(route_to('bv_in_out_manager'));
        }
        return $this->render('index');

    }
}