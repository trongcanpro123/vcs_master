<?php

namespace App\Controllers\Admin;


use App\Controllers\BaseController;
use App\Helpers\SettingHelper;
use App\Models\AdministratorModel;
use App\Models\CarInOutHistoryModel;
use App\Helpers\SessionHelper;
use App\Models\PumpProductTypeModel;
use App\Models\PumpSystemModel;
use App\Models\PumpThroadModel;
use App\Models\AreaModel;
use App\Models\TgbxOrderDetailModel;
use App\Models\TgbxOrderDetailSortModel;
use App\Models\TgbxOrderModel;
use App\Models\VoiceOptionModel;
use App\Models\VoiceSortModel;
use App\Models\CarSortModel;
use CodeIgniter\Model;
use DateTime;
class Operator extends BaseController
{
    public function index()
    {

//        $this->layout = 'empty';
        return $this->render('operator/order_take_ticket');
    }

    public function order_take_ticket()
    {

        if ($this->check_permission_for('nhan_vien') && $this->check_permission_for('lanh_dao_kho')){
            $link_page = $this->check_permission_for('nhan_vien')? $this->check_permission_for('nhan_vien') : $this->check_permission_for('lanh_dao_kho');
            return $this->response->redirect($link_page);
        }

        $admin = AdministratorModel::findIdentity();
        $area_id = $admin->area_id;
        $data = $this->request->getGet();
        $models = (new CarInOutHistoryModel())->where('area_id', $area_id)
            ->where('ticket', 0)
            ->where('checkin_order > ', 0 )
            ->where('ticket_order > ', 0 )
            ->where('checkout_time IS NULL' )
            ->where('ticket_alert' , 0);

        $driver_name = $data['driver_name'];
        $car_number = $data['car_number'];
        $param_search = [];
        if ($driver_name) {
            $models->like('driver_name', $driver_name);
            $param_search['driver_name'] = $driver_name;
        }
        if ($car_number) {
            $models->like('car_number', $car_number);
            $param_search['car_number'] = $car_number;
        }

        $models->orderBy('ticket_order ASC,updated_at ASC', '', true);
        // echo '<pre>'; print_r($models);die;
        $list_item_wait_ticket = (new CarInOutHistoryModel())->where('area_id', $area_id)
            ->where('ticket',0)
            ->where('checkin_order > ', 0 )
            ->where('ticket_alert' , 1)
            ->where('checkout_time IS NULL')
            ->orderBy('updated_at','ASC')->findAll();

        $list_item_in_alert = (new CarInOutHistoryModel())->get_list_in_out_in_alert_take_ticket($area_id,TAKE_TICKET_ALERT_MINUTES);

        return $this->render('operator/order_take_ticket', [
                'models' => $models->findAll(),
                'param_search' => $param_search,
                'list_item_wait_ticket' => $list_item_wait_ticket,
                'list_item_in_alert' => $list_item_in_alert,
                'area_id' => $area_id,
            ]
        );

    }

    public function set_down_take_ticket($id)
    {
        $model = (new CarInOutHistoryModel())->where('id', $id)->first();

        if ($model) {
//            $query_find_near = 'select id, checkin_order from  car_in_out_history where area_id = ? and ticket = 0 and checkin_order >= ? ';
//            $row_down_near = $this->db->query($query_find_near,[$model->area_id, $model->checkin_order])->row();
            $row_down_near = (new CarInOutHistoryModel())->getRowNearDown($model->area_id, $model->ticket_order);
            if ($row_down_near) {
                (new CarInOutHistoryModel())->update($id, ['ticket_order' => $row_down_near->ticket_order]);

                (new CarInOutHistoryModel())->update($row_down_near->id, ['ticket_order' => $model->ticket_order]);

            }
        }
        return $this->response->redirect(route_to('nv_order_take_ticket'));


    }
    public function process_reject_get_ticket($id){
        $model = (new CarInOutHistoryModel())->where('id', $id)->first();
        if($model && !$model->ticket_alert){
            (new CarInOutHistoryModel())->update($id,[
                'note' => 'Hủy không lấy ticket',
                'ticket_order' => -1
            ]);
        }
        return $this->response->redirect(route_to('nv_order_take_ticket'));
    }
    public function process_alert_get_ticket($id){
        // $model = (new CarInOutHistoryModel())->where('id', $id)->first();
        // return json_encode(1);
        echo '<pre>'; print_r("asdsadsadsads");die;
        // if($model && !$model->ticket_alert){
        //     return json_encode(1);
        //     // (new CarInOutHistoryModel())->update($id,[
        //     //     'ticket_alert' => 1
        //     // ]);
        // }
        return $this->response->redirect(route_to('nv_order_take_ticket'));
    }
    public function process_return_back_ticket($id){
        $model = (new CarInOutHistoryModel())->where('id', $id)->first();
        if($model && $model->ticket_alert){
            (new CarInOutHistoryModel())->reset_order_get_ticket($id,$model->area_id);
        }
        return $this->response->redirect(route_to('nv_order_take_ticket'));
    } 
    public function process_update_note($id) {
        $data_post = $this->request->getPost();
        $model = (new CarInOutHistoryModel())->where('id', $id)->first();
        (new CarInOutHistoryModel())->update($model->id,[
            'note' => $data_post['note']
        ]);
        return json_encode($model);
    }
    public function process_get_ticket($id){
        $model = (new CarInOutHistoryModel())->where('id', $id)->first();
	// echo '<pre>'; print_r($model);die;
        // $area_code = $model->area_id == '1' ? 'nghi_huong' : 'ben_thuy';
        $setting = new SettingHelper();
       // $checkin_time = DateTime::createFromFormat('d-m-Y H:i:s', $model->checkin_time)->format('Y-m-d H:i:s');
       // echo '<pre>'; print_r($checkin_time);die;
        // $checkin_time = (new DateTime($model->checkin_time))->format('Y-m-d H:i:s');
       // call function  Khi check có ticket chưa thì gọi: [dbo].[VCS_Check_Ticket_BenThuy] p_BienSoXe, p_CheckIn_Time
        $api_vcs_tiket = $setting->api_vcs_tiket($model->car_number,$model->checkin_time,$model->area_id);
      //  echo '<pre>'; print_r($api_vcs_tiket['data']);die;

        if($api_vcs_tiket) {
            $result = $api_vcs_tiket['data'];
            if($result == 0) {
                return json_encode(0);
            } else {
                $api_vcs_arrange = $setting->api_vcs_arrange($model->car_number, $model->id,$model->area_id);
                if($api_vcs_arrange) {
                    $data = $api_vcs_arrange['data'];
                    if($data == 0) {
                        (new CarInOutHistoryModel())->update($id,[
                            'ticket_alert' => 0
                        ]);
                    return json_encode($api_vcs_arrange);
                    } else {
                        (new CarInOutHistoryModel())->update($id,[
                            'ticket' => 1,
                            'invite_take_item' => 1,
                            'ticket_alert' => 1
                        ]);
                        //call thu tuc get du lieu sap xep xe .
 			// echo '<pre>'; print_r($model->area_id);die;
                        $api_vcs_car_arrangement = $setting->api_vcs_car_arrangement($model->area_id);
 			// echo '<pre>'; print_r($api_vcs_car_arrangement);die;
			(new CarSortModel())->truncate();
                        if($api_vcs_car_arrangement) {
                            foreach ($api_vcs_car_arrangement as $item_detail){
			 //echo '<pre>'; print_r($item_detail['id']);die;                   
//                 (new CarSortModel()) -> delete();
					try {
				//	(new CarSortModel())->truncate();
                                        (new CarSortModel())->insert([
                                        'id_smo' => $item_detail['id'],
                                       'ngay_xuat' => $item_detail['NgayXuat'],
                                        'phuong_tien' => $item_detail['PhuongTien'],
                                        'so_lenh' => $item_detail['SoLenh'],
                                        'ma_hang_hoa' => $item_detail['MaHangHoa'],
                                        'ten_hang_hoa' => $item_detail['TenHangHoa'],
                                        'trang_thai' => $item_detail['TrangThai'],
                                        'pump_system_id' => $item_detail['pump_system_id'],
                                        'pump_system_name' => $item_detail['pump_system_name'],
                                        'throad_id' => $item_detail['throad_id'],
                                       'throad_name' => $item_detail['throad_name'],
                                        'display_on' => $item_detail['display_on'],
                                        'car_in_out_id' => $item_detail['car_in_out_id'],
                                        'update_time' => $item_detail['update_time'],
                                        'sequence_time' => $item_detail['sequence_time'],
                                        'process_status' => $item_detail['process_status'],
                                        'area_id' => $item_detail['area_id'],
                                    ]);
					SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'info',
                'message' => 'Cập nhật thành công'
            ]);

					 } catch (\Exception $ex) {
            SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'danger',
                'message' => $ex->getMessage()
            ]);
        }
                        } }
	 $api_vcs_process = $setting->api_vcs_process($model->area_id);
                    //     if($api_vcs_car_arrangement) {
                    //         foreach ($api_vcs_car_arrangement['data'] as $item_detail){
                    ///                    (new TgbxOrderDetailModel())->insert([
                    //                         'ma_lenh' => $item_detail['MaLenh'],
                    //                         'so_lenh' => $item_detail['SoLenh'],
                    //                         'line_id' => $item_detail['LineID'],
                    //                         'tong_du_xuat' => $item_detail['TongDuXuat'],
                    //                         'ma_hang_hoa' => $item_detail['MaHangHoa'],
                    //                         'ngay_xuat' => $order_info['NgayXuat'],
                    //                         'ma_phuong_tien' => $order_info['MaPhuongTien'],
                    //                         'nguoi_van_chuyen' => $order_info['NguoiVanChuyen'],
                    //                         'area_id' => $model->area_id,
                    //                         'tdh_status' => 'dang_cho',
                    //                         'check_time' => (new \DateTime())->format('Y-m-d H:i:s'),
                    //                         'in_out_id' => $id,
                    //                         'order_id' => $tgbx_order_id
                    //                     ]);
                                    
                    //     (new TgbxOrderDetailSortModel())->insert([
                    //     'tgbx_detail_id' => $key,
                    //     'pump_system_id' => $throad_info->pump_system_id,
                    //     'throad_id' => $throad_info->id,
                    //     'pump_system_name' => $throad_info->select_pump_system(),
                    //     'throad_name' => $throad_info->throad_name,
                    //     'item_name' => $tgbx_detail->get_ten_hang(),
                    //     'display_on' => $throad_info->display_on,
                    //     'in_out_id' => $id
                    //     ]);
                    //     }
                    // }

                    return json_encode($api_vcs_arrange);
                    }
                }
            }
        }
        //  vcs-tiket
        // if(api_data = 0) {
        //     return = 0 
        //     muc dich bat alert thong bao chua co ticket
        // } else {
        //     call function 1. Khi xe được vào sắp xếp thì gọi: [dbo].[VCS_Process_BenThuy] p_BienSoXe, p_In_Out_ID
        //     vcs-arrange
        //     neu tra ve 1 thi thanh cong {
        //        'ticket' => 1,
        //             'invite_take_item' => 1
        // ticket_alert = 1
        //     }
            

        //     neu bang 0 thi cap nhat lai ticket_alert' => 0
        // }

        // if($api_data ){
        //     $list_order = $api_data['list_order'];
        //     //check throad active with order
        //     if($list_order && count($list_order)){
        //         foreach ($list_order as $bx_order){
        //             $order_info = $bx_order['order_info'];
        //             $order_detail = $bx_order['order_detail'];

        //             foreach ($order_detail as $item_detail){
        //                $list_throad =   (new TgbxOrderDetailModel())->get_list_throad_wait_time($model->area_id,$item_detail['MaHangHoa']);

        //                if(!$list_throad || count($list_throad) == 0){
        //                    return json_encode(-1); //lỗi cấu hình họng
        //                }
        //             }
        //         }
        //     }



        //     if($list_order && count($list_order)){
        //         //save db order

        //         foreach ($list_order as $bx_order){

        //             $order_info = $bx_order['order_info'];
        //             $order_detail = $bx_order['order_detail'];

        //             $tgbx_order_id = (new TgbxOrderModel())->insert([
        //                 'ma_lenh' => $order_info['Malenh'],
        //                 'so_lenh' => $order_info['SoLenh'],
        //                 'ngay_xuat' => $order_info['NgayXuat'],
        //                 'ma_phuong_tien' => $order_info['MaPhuongTien'],
        //                 'nguoi_van_chuyen' => $order_info['NguoiVanChuyen'],
        //                 'status' => $order_info['Status'],
        //                 'area_id' => $model->area_id,
        //                 'in_out_id' => $id
        //             ],true);



        //             foreach ($order_detail as $item_detail){
        //                 (new TgbxOrderDetailModel())->insert([
        //                     'ma_lenh' => $item_detail['MaLenh'],
        //                     'so_lenh' => $item_detail['SoLenh'],
        //                     'line_id' => $item_detail['LineID'],
        //                     'tong_du_xuat' => $item_detail['TongDuXuat'],
        //                     'ma_hang_hoa' => $item_detail['MaHangHoa'],
        //                     'ngay_xuat' => $order_info['NgayXuat'],
        //                     'ma_phuong_tien' => $order_info['MaPhuongTien'],
        //                     'nguoi_van_chuyen' => $order_info['NguoiVanChuyen'],
        //                     'area_id' => $model->area_id,
        //                     'tdh_status' => 'dang_cho',
        //                     'check_time' => (new \DateTime())->format('Y-m-d H:i:s'),
        //                     'in_out_id' => $id,
        //                     'order_id' => $tgbx_order_id
        //                 ]);
        //             }



        //         }
        //         //
        //         $list_phuong_an = $this->process_get_solution_throad_for_in_out($model->area_id,$id);
        //         $min_phuong_an = $list_phuong_an[0];
        //         foreach($list_phuong_an as $phuong_an){
        //             if($phuong_an['tong_thoi_gian']< $min_phuong_an['tong_thoi_gian']){
        //                 $min_phuong_an = $phuong_an;
        //             }
        //         }

        //         foreach($min_phuong_an['data'] as $key => $throad_id){
        //             $throad_info = (new PumpThroadModel())->where('id',$throad_id)->first();
        //             (new TgbxOrderDetailModel())->update($key,[
        //                 'throad_id' => $throad_info->id,
        //                 'throad_name' => $throad_info->throad_name,
        //                 'pump_system_id' => $throad_info->pump_system_id,
        //                 'pump_name' => $throad_info->select_pump_system()
        //             ]);

        //             //hien thi
        //             $tgbx_detail = (new TgbxOrderDetailModel())->where('id',$key)->first();

        //             (new TgbxOrderDetailSortModel())->insert([
        //                 'tgbx_detail_id' => $key,
        //                 'pump_system_id' => $throad_info->pump_system_id,
        //                 'throad_id' => $throad_info->id,
        //                 'pump_system_name' => $throad_info->select_pump_system(),
        //                 'throad_name' => $throad_info->throad_name,
        //                 'item_name' => $tgbx_detail->get_ten_hang(),
        //                 'display_on' => $throad_info->display_on,
        //                 'in_out_id' => $id
        //             ]);

        //             $order_id = (new VoiceSortModel())->insert([
        //                 'tgbx_detail_id' => $key,
        //                 'pump_system_id' => $throad_info->pump_system_id,
        //                 'throad_id' => $throad_info->id,
        //                 'pump_system_name' => $throad_info->select_pump_system(),
        //                 'throad_name' => $throad_info->throad_name,
        //                 'item_name' => $tgbx_detail->get_ten_hang(),
        //                 'display_on' => $throad_info->display_on,
        //                 'in_out_id' => $id,
        //                 'alert_message' => VoiceSortModel::generate_alert_message($throad_info->throad_name, $id)
        //             ],true);
        //             (new VoiceSortModel())->update($order_id,['
        //             alert_order' => $order_id]);
        //         }

        //         //update ticket & kết hợp mời vào lấy hàng luôn
        //         (new CarInOutHistoryModel())->update($id,[
        //             'ticket' => 1,
        //             'invite_take_item' => 1
        //         ]);
        //         return json_encode($model->checkin_order);
        //     }


        // }
        return json_encode(-1);

    }

    public function process_get_solution_throad_for_in_out($area_id,$in_out_id){
        $list_order_detail_db = (new TgbxOrderDetailModel())->where('in_out_id',$in_out_id)->findAll();

        $list_phuong_an = [];
        //phuong an bom roi rac
        $tong_tg_cho = 0;
        $data_pa = [];
        if($list_order_detail_db){
            foreach ($list_order_detail_db as $item_detail_db){
                $throad_estimate = $this->get_minimum_throad_time($area_id,$item_detail_db->ma_hang_hoa,$item_detail_db->tong_du_xuat);
                $tong_tg_cho += $throad_estimate['wait_time'];
                $data_pa[$item_detail_db->id] = $throad_estimate['id'];
            }
        }

        $list_phuong_an[] = [
                'name' => 'roi_rac',
                'tong_thoi_gian' => $tong_tg_cho,
                'data' => $data_pa
        ];

        $tong_tg_cho = 0;
        $data_pa = [];
        //cac hong song song ton tai trong don
        $list_pararell_in_order = (new TgbxOrderDetailModel())->get_list_throad_pararell_of_order($area_id,$in_out_id);

        if($list_pararell_in_order && $list_order_detail_db){
            foreach ($list_order_detail_db as $item_detail_db){
                foreach ($list_pararell_in_order as $pararell){
                    if($item_detail_db->ma_hang_hoa == $pararell['product_type_code']){
                        if(!$data_pa[$item_detail_db->id]){ //chua co du lieu
                            $data_pa[$item_detail_db->id] = $pararell['id'];
                            if($pararell['calc']){
                                if($pararell['wattage']){
                                    $tong_tg_cho += ($item_detail_db->tong_du_xuat +  $pararell['tong_so_luong'])/$pararell['wattage'];

                                }
                            }
                        }

                    }
                }
            }
        }

        if($list_order_detail_db){
            foreach ($list_order_detail_db as $item_detail_db){
                if(!$data_pa[$item_detail_db->id]){
                    $throad_estimate = $this->get_minimum_throad_time($area_id,$item_detail_db->ma_hang_hoa,$item_detail_db->tong_du_xuat);
                    $tong_tg_cho += $throad_estimate['wait_time'];
                    $data_pa[$item_detail_db->id] = $throad_estimate['id'];
                }
            }
        }

        if(count($data_pa) > 0){
            $list_phuong_an[] = [
                    'name' => 'song_song',
                    'tong_thoi_gian' => $tong_tg_cho,
                    'data' => $data_pa
            ];
        }


        return $list_phuong_an;
    }

    public function get_minimum_throad_time($area_id,$ma_mat_hang,$so_luong){
        $list_throad = (new TgbxOrderDetailModel())->get_list_throad_wait_time_estimate($area_id,$ma_mat_hang,$so_luong);
        if(!$list_throad || count($list_throad) == 0){
            return false;
        }
        $min_throad = $list_throad[0];
        foreach ($list_throad as $throad){
            if($throad['wait_time'] < $min_throad['wait_time']){
                $min_throad = $throad;
            }
        }
        return  $min_throad;
    }

    public function process_set_alert_ticket(){
        $nhan_vien = AdministratorModel::findIdentity();

        $list_item_wait_alert = (new CarInOutHistoryModel())->where('area_id', $nhan_vien->area_id)
            ->where('ticket',1)
            ->where('checkin_order > ', 0 )
            ->where('ticket_alert' , 0)
            ->orderBy('updated_at','ASC')->findAll();

        if($list_item_wait_alert && count($list_item_wait_alert)){
            foreach ($list_item_wait_alert as $item){
                (new CarInOutHistoryModel())->update($item->id,[
                    'ticket_alert' => 1
                ]);
            }
        }

        return $this->response->redirect(route_to('nv_order_take_ticket'));
    }

    public function take_ticket_alert($area_id){

        $list_checkin_wait = (new CarInOutHistoryModel())->where('area_id',$area_id)
            ->where('ticket', 0)
            ->where('checkin_order > ', 0 )
            ->where('ticket_order > ', 0 )
            ->where('ticket_alert', 0 )
            ->where('checkout_time IS NULL')
            ->orderBy('ticket_order ASC,updated_at ASC','', true)->findAll(20);
        $list_item_in_alert = (new CarInOutHistoryModel())->get_list_in_out_in_alert_take_ticket($area_id,TAKE_TICKET_ALERT_MINUTES);
        return $this->view("quantri/operator/take_ticket_alert", [
            'list_wait' => $list_checkin_wait,
            'list_alert' => $list_item_in_alert
        ]);
    }

    public function print_ticket()
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        $nhan_vien = AdministratorModel::findIdentity();
        $area_id = $nhan_vien->area_id;
        $model = null;
        $list_tgbx_detail = null;
        $data = $this->request->getGet();
        $car_number = $data['car_number'];
        $param_search = [];
        if ($car_number) {
            $param_search['car_number'] = $car_number;

            $model = (new CarInOutHistoryModel())
                ->where('car_number',$car_number )
                ->where('checkin_order >',0)
                ->where('checkout_time IS NULL')
                ->where('area_id',$area_id)
                ->orderBy('id', 'DESC')->first();
            if($model){
                $list_tgbx_detail = (new TgbxOrderDetailModel())->where('in_out_id',$model->id)->findAll();
            }
        }
        if(!$model && $car_number){
            SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'warning',
                'message' => 'Không tìm thấy thông tin vào ra của xe'
            ]);
        }
        return $this->render('operator/print_ticket',[
            'model' => $model,
            'list_tgbx_detail' => $list_tgbx_detail,
            'param_search' => $param_search
        ]);

    }

    public function order_take_item()
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        $nhan_vien = AdministratorModel::findIdentity();
        $data = $this->request->getGet();
        $check_all = $data['check_all'];

        $list_in_out_take_item = null;
        if($check_all){
            $list_in_out_take_item = (new CarInOutHistoryModel())->where('area_id',$nhan_vien->area_id)
                ->where('ticket' ,1)
                ->where('invite_take_item', 1)
                ->where('checkout_time IS NULL')
                ->orderBy('ticket_order ASC, updated_at ASC')
                ->findAll();
        }else{
            $list_in_out_take_item = (new CarInOutHistoryModel())->where('area_id',$nhan_vien->area_id)
                ->where('ticket' ,1)
                ->where('invite_take_item', 1)
                ->where('checkout_order_status',0)
                ->where('checkout_time IS NULL')
                ->orderBy('ticket_order ASC, updated_at ASC')
                ->findAll();
        }



        return $this->render('operator/order_take_item',[
            'list_in_out_take_item' => $list_in_out_take_item,
            'check_all' => $check_all,
            'nhan_vien' => $nhan_vien
        ]);

    }

    public function invite_take_item($id){
       $model =  (new CarInOutHistoryModel())->where('id',$id)->first();
       if($model){
           (new CarInOutHistoryModel())->update($id,[
              'invite_take_item' => 1
           ]);
           SessionHelper::getInstance()->setFlash('ALERT', [
               'type' => 'success',
               'message' => 'Đã mời xe vào lấy hàng thành công'
           ]);
       }
       return $this->response->redirect(route_to('nv_order_take_item'));
    }

    public function push_up_order_take_item($id){
        $model =  (new CarInOutHistoryModel())->where('id',$id)->first();
        if($model){
            $list_waiting = (new CarInOutHistoryModel())->where('area_id',$model->area_id)
                ->where('ticket' ,1)
                ->where('invite_take_item', 0)
                ->where('checkout_time IS NULL')
                ->orderBy('ticket_order ASC, updated_at ASC')
                ->findAll();
            if($list_waiting && count($list_waiting)>1){
                $up_index = -1;
                for($i=0; $i<count($list_waiting) ; $i++){
                    if (($list_waiting[$i]->id == $model->id) && $i > 0){
                        $up_index = $i - 1;
                        break;
                    }
                }

                $model_up = $list_waiting[$up_index];

                (new CarInOutHistoryModel())->update($model->id,[
                   'ticket_order' => $model_up->ticket_order
                ]);
                (new CarInOutHistoryModel())->update($model_up->id,[
                    'ticket_order' => $model->ticket_order
                ]);
            }

            SessionHelper::getInstance()->setFlash('ALERT', [
                'type' => 'success',
                'message' => 'Đã xử lý ưu tiên xe thành công'
            ]);
        }
        return $this->response->redirect(route_to('nv_order_take_item'));
    }

    public function take_item_alert($area_id, $display_on){

        $list_alert = (new TgbxOrderDetailSortModel())->get_list_take_item_waiting_alert($area_id,$display_on,TAKE_ITEM_ALERT_MINUTES);
  
        $list_throad = (new PumpThroadModel())->get_list_throad_alert($area_id,$display_on);
        return $this->view("quantri/operator/take_item_alert", [
            'list_alert' => $list_alert,
            'list_throad' => $list_throad
        ]);
    }

    public function scan_update_item_status(){      
 $list_in_out = (new CarInOutHistoryModel())->where('ticket' ,1)
            ->where('checkout_time IS NULL')
            ->findAll();

        $setting = (new SettingHelper());
        foreach ($list_in_out as $in_out){
            $area_code = '';
            if($in_out->area_id == '1'){
                $area_code = 'nghi_huong';
            }
            if($in_out->area_id == '2'){
                $area_code = 'ben_thuy';

            }
            $checkout_order_status_done = true;
            $list_tgbx_detail = (new TgbxOrderDetailModel())->where('in_out_id',$in_out->id)->findAll();
            foreach ($list_tgbx_detail as $tgbx){

                $trang_thai = $setting->api_tdh_check_status($in_out->car_number,$area_code,$tgbx->ma_lenh,$tgbx->ngay_xuat,$tgbx->ma_hang_hoa);
               
		 if($trang_thai == 'dang_xuat'){
                    (new TgbxOrderDetailModel())->update($tgbx->id,[
                        'tdh_status' => $trang_thai,
                        'check_time' => (new \DateTime())->format('Y-m-d H:i:s')
                    ]);
                    $checkout_order_status_done = $checkout_order_status_done && false;
                }
                if($trang_thai == 'ket_thuc'){
                    (new TgbxOrderDetailModel())->update($tgbx->id,[
                        'tdh_status' => $trang_thai,
                        'check_time' => (new \DateTime())->format('Y-m-d H:i:s')
                    ]);
                    //remove sort table
                    $sort = (new TgbxOrderDetailSortModel())->where('tgbx_detail_id',$tgbx->id)->first();
                    if($sort){
                        (new TgbxOrderDetailSortModel())->delete($sort->id);
                    }
                }else{
                    (new TgbxOrderDetailModel())->update($tgbx->id,[
                        'check_time' => (new \DateTime())->format('Y-m-d H:i:s')
                    ]);
                    $checkout_order_status_done = $checkout_order_status_done && false;
                }
            }

            if($checkout_order_status_done){
                (new CarInOutHistoryModel())->update($in_out->id,
                    [
                        'checkout_order_status' => 1
                    ]
                );
            }

        }
    }
    // cấu hình///////////////////
    public function config()
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        $session = Session();
        $user_id = $session->get('PANDA_ADMIN');
        $model_admin = (new AdministratorModel())->select_area_admin_id($user_id['id']);
        $area_id = $model_admin->area_id;

        $models_pumpthroad = (new PumpThroadModel())
            ->where('area_id', $area_id);
        return $this->render('operator/config', [
            'models' => $models_pumpthroad->paginate(),
            'pager' => $models_pumpthroad->pager
        ]);

    }

    public function config_create()
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        $session = Session();
        $user_id = $session->get('PANDA_ADMIN');
        $model_admin = (new AdministratorModel())->select_area_admin_id($user_id['id']);

        $area_id = $model_admin->area_id;
        $model = new PumpThroadModel();
        $model_throad = (new PumpThroadModel())->findAll();

        $model_pump_system = (new PumpSystemModel())
            ->where('area_id', $area_id)
            ->findAll();

        $model_area = (new AreaModel())->select_area($user_id['id']);

        $model_pump_prd = (new PumpProductTypeModel())->findAll();

        if ($this->isPost() && $this->validate($model->getRules())) {
            $data_post = $this->request->getPost();
            $exist = (new PumpThroadModel())->where('throad_name',$data_post['throad_name'])->where('area_id',$area_id)->first();
            if($exist){

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => 'Đã tồn tại tên họng này'
                ]);

                return $this->response->redirect('/quantri/operator/create');
            }

            try {
                $model->loadAndSave($this->request, function ($request, array $data) {
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'success',
                    'message' => 'Thêm mới thành công'
                ]);

                return $this->response->redirect(route_to('nv_config'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }
        }


        return $this->render('operator/config_create', [
            'model' => $model,
            'model_pump_system' => $model_pump_system,
            'model_pump_prd' => $model_pump_prd,
            'model_throad' => $model_throad,
            'model_area' => $model_area,
            'validator' => $this->validator
        ]);
    }

    public function select_throad($pump_system_id)
    {
        $session = Session();
        $user_id = $session->get('PANDA_ADMIN');
        $model_admin = (new AdministratorModel())->select_area_admin_id($user_id['id']);
        $area_id = $model_admin->area_id;
        $model = (new PumpThroadModel())->select_pump_throad_system($pump_system_id, $area_id);
        echo json_encode($model);
    }


    public function config_update($id)
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        $session = Session();
        $user_id = $session->get('PANDA_ADMIN');
        $model_admin = (new AdministratorModel())->select_area_admin_id($user_id['id']);
        $area_id = $model_admin->area_id;
        $model = (new PumpThroadModel())->find($id);
        $model_area = (new AreaModel())->select_area($user_id['id']);
        $pump_system = $model->pump_system_id;
        $product_type = $model->product_type_id;
        $thread_pararell = $model->thread_pararell_id;
        $area_name = $model->area_id;
        $model_throad = (new PumpThroadModel())
            ->where('area_id', $area_id)
            ->where('pump_system_id', $pump_system)
            ->findAll();
        $model_pump_system = (new PumpSystemModel())
            ->where('area_id', $area_id)
            ->findAll();

        $model_pump_prd = (new PumpProductTypeModel())->findAll();

        if (!$model) {
            return $this->renderError();
        }

        if ($this->isPost() && $this->validate($model->getRules())) {

            $data_post = $this->request->getPost();
            $exist = (new PumpThroadModel())->where('id <> ',$id)
                ->where('throad_name',$data_post['throad_name'])->where('area_id',$area_id)->first();
            if($exist){
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => 'Đã tồn tại tên họng này'
                ]);
                return $this->response->redirect('/quantri/operator/update/'.$id);
            }

            try {
                $model->loadAndSave($this->request, function ($request, array $data) {
                    return $data;
                });

                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'info',
                    'message' => 'Cập nhật thành công'
                ]);

                return $this->response->redirect(route_to('nv_config'));
            } catch (\Exception $ex) {
                SessionHelper::getInstance()->setFlash('ALERT', [
                    'type' => 'danger',
                    'message' => $ex->getMessage()
                ]);
            }

        }

        return $this->render('operator/config_update', [
            'model' => $model,
            'model_area' => $model_area,
            'pump_system' => $pump_system,
            'product_type' => $product_type,
            'thread_pararell' => $thread_pararell,
            'model_pump_system' => $model_pump_system,
            'model_pump_prd' => $model_pump_prd,
            'model_throad' => $model_throad,
            'area_name' => $area_name,
            'validator' => $this->validator
        ]);
    }

    public function select_throad_update($pump_system_id)
    {
        $session = Session();
        $user_id = $session->get('PANDA_ADMIN');
        $model_admin = (new AdministratorModel())->select_area_admin_id($user_id['id']);
        $area_id = $model_admin->area_id;
        $model = (new PumpThroadModel())->select_pump_throad_system_update($pump_system_id, $area_id);
        echo json_encode($model);
    }

    public function config_delete($id)
    {
        if ($this->check_permission_for('nhan_vien')){
            return $this->response->redirect($this->check_permission_for('nhan_vien'));
        }

        /** @var PumpThroadModel $model */
        if (!$this->isPost() || !($model = (new PumpThroadModel())->find($id))) {
            return $this->renderError();
        }

        SessionHelper::getInstance()->setFlash('ALERT', [
            'type' => 'warning',
            'message' => 'Xoá thành công'
        ]);
        $model->delete($model->getPrimaryKey());
        return $this->response->redirect(route_to('nv_config'));
    }

    public function help()
    {
        return $this->render('operator/help');

    }
 
    public function get_list_voice_of_throad($area_id, $display_on)
    {
        $list_throad = (new PumpThroadModel())->get_list_throad_alert($area_id, $display_on);
 
        session_start();
        $current_throad_index = $_SESSION['current_throad'];
        $current_voice_index  = $_SESSION['current_voice'];

        if(is_null($current_throad_index)) {
            $current_throad_index = 0;
        }else {
            $current_throad_index += 1;
        }

        $_SESSION['current_throad'] = $current_throad_index;
        $current_throad = $list_throad[$current_throad_index]; 
        $list_voice_wait = (new VoiceSortModel())->get_list_voice_waiting_on_throad($current_throad['id']);

        if(is_null($current_voice_index)) {
            $current_voice_index = 0;
        }

        $current_voice_wait = $list_voice_wait[$current_voice_index];

        $tgbx_info = (new TgbxOrderDetailModel())->where('id', $current_voice_wait['tgbx_detail_id'])->first();
       
        if(!empty($current_voice_wait) && $current_voice_wait['alert_count'] < '2') {
            $voice_id = $current_voice_wait['id'];
            $this->update_play_voice_status_test($voice_id);
        } else if($current_voice_wait['alert_count'] == '2') {
                $current_voice_wait = null;
        }
        // }else if($current_voice_wait['alert_count'] == '2' && !empty($tgbx_info) && $tgbx_info->tdh_status == 'dang_cho') {
        //     $current_voice_wait = null;
        // }

        if($current_throad_index == count($list_throad)) {
            session_destroy();
        }
        echo '<pre>'; print_r($current_voice_wait);die;

        return json_encode($current_voice_wait);
    }

    public function check_voice_status_after_time($area_id, $display_on) {

        $list_throad = (new PumpThroadModel())->get_list_throad_alert($area_id, $display_on);

        foreach($list_throad as $throad) {
            $list_voice_wait = (new VoiceSortModel())->get_list_voice_play_twice($throad['id']);
            foreach($list_voice_wait as $voice_wait) {
                // check lan cuoi phat loa da duoc 10p chua
                $alert_last_time = $voice_wait['alert_last_time'];
                $current_time    = date('Y-m-d H:i:s');
                $time_check      = date(strtotime('Y-m-d H:i:s', $alert_last_time + '10 minute'));
                if($current_time == $time_check) {
                    $tgbx_info = (new TgbxOrderDetailModel())->where('id', $voice_wait['tgbx_detail_id'])->first();
                    // check sau 10p goi ma xe k vao lay hang
                    if(!empty($tgbx_info) && $tgbx_info['tdh_status'] == 'dang_cho') {
                        // move xe nay ve cuoi hang bang cach update alert_order = lớn nhất.
                        $alert_order_value = (new VoiceSortModel())->get_max_value_alert_order()[0]['alert_order'];
                        $alert_order_value += 1;
                        (new VoiceSortModel())->update($voice_wait['id'], [
                            'alert_order' => $alert_order_value
                        ]);
                    }
                }
            }
        }
    }

    public function check_car_is_waiting_different_throad($in_out_id, $voice_id) {
       return (new VoiceSortModel())->get_voice_waiting_at_different_throad($in_out_id, $voice_id);
    }

    public function update_play_voice_status_test($voice_id) {
        $old_data = (new VoiceSortModel())->where('id', $voice_id)->first();
        (new VoiceSortModel())->update($voice_id,[
            'alert_last_time' => (new \DateTime())->format('Y-m-d H:i:s'),
            'alert_count' => ($old_data->alert_count + 1)
        ]);
        return json_encode('normal');
    }
    public function get_list_voice_of_throad_test($area_id, $display_on){
        $voice_config = (new VoiceOptionModel())->first();
        $time_wait_come_in = 5;
        if($voice_config){
            $time_wait_come_in = $voice_config->time_wait_come_in;
        }
        $list_throad = (new PumpThroadModel())->get_list_throad_alert($area_id,$display_on);
 
        $list_voice_play = [];
        foreach ($list_throad as $throad){
            $list_voice_wait = (new VoiceSortModel())->get_list_voice_waiting_on_throad($throad['id']);
            foreach ($list_voice_wait as $voice_wait){
                if($voice_wait['alert_count'] == 2){
                    $tgbx_info = (new TgbxOrderDetailModel())->where('id',$voice_wait['tgbx_detail_id'])->first();

                    //chua play back ma dang cho
                    if($tgbx_info && $tgbx_info->tdh_status == 'dang_cho' && !$voice_wait['alert_back']){
                        //truong hop cho 5"
                        $now =(new \DateTime()) ;

                        if($voice_wait['alert_last_time'] && ($voice_wait['alert_last_time'] < $now)){
                            $interval = date_diff(date_create_from_format('Y-m-d H:i:s',$voice_wait['alert_last_time']), $now)->format('%i');

                            if($interval < $time_wait_come_in){
                                //khong add gì vao playlist ma chi doi

                                break;
                            }else{
                                //update play back
                                //find next 2 element id
                                $next_index = -1;
                                $next_id = $voice_wait['id'];
                                foreach ($list_voice_wait as $next_elm) {
                                    if($next_index >= 0 && $next_index <2){
                                        $next_index++;
                                        $next_id = $next_elm['id'];
                                    }
                                    if($next_elm['id'] == $voice_wait['id']){
                                        $next_index = 0;
                                    }
                                }
                                (new VoiceSortModel())->update($voice_wait['id'],[
                                    'alert_back' => '1',
                                    'alert_order' => $next_id
                                ]);
                                (new VoiceSortModel())->update($next_id,[
                                    'alert_order' => $voice_wait['id']
                                ]);
                                continue;
                            }
                        }
                    }
                }

                $list_voice_play[] = $voice_wait;
                break;
            }
        }
        return json_encode($list_voice_play);
    }

    public function update_play_voice_status($voice_id){
        $old_data = (new VoiceSortModel())->where('id',$voice_id)->first();
        if(!$old_data->alert_back){
            (new VoiceSortModel())->update($voice_id,[
                'alert_last_time' => (new \DateTime())->format('Y-m-d H:i:s'),
                'alert_count' => ($old_data->alert_count + 1)
            ]);
            return json_encode('normal');
        }else{
            (new VoiceSortModel())->update($voice_id,[
                'alert_last_time' => (new \DateTime())->format('Y-m-d H:i:s'),
                'alert_count_2' => ($old_data->alert_count_2 + 1)
            ]);
            return json_encode([$old_data->alert_count_2 + 1,$voice_id,'alert_back']);
        }

    }

    public function take_item_alert_voice($area_id, $display_on){

        return $this->view("quantri/operator/take_item_alert_voice", [
            'area_id' => $area_id,
            'display_on' => $display_on
        ]);
    }

    public function get_config_voice(){
        $config = (new VoiceOptionModel())->first();
        if($config){
            return json_encode([
                'number_alert' => $config->number_alert,
                'alert_time_loop' => $config->alert_time_loop
            ]);
        }
        return json_encode(null);
    }

}
