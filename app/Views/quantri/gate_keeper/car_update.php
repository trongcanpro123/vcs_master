<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectCategoryModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Cập nhật xe';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-info flex-align">
                <div>
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>
            </div>
            <div class="card-body">
                <form action="<?= route_to('admin_car_update', $model->getPrimaryKey()) ?>" method="post"
                      enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card" style="margin-bottom: 0px">
                                        <div class="card-header card-header-text card-header-info">
                                            <div class="card-text">
                                                <h6 class="card-title">Chọn camera</h6>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="">
                                                        <select class="form-control " data-style="btn btn-link"
                                                                id="select_camera_in"
                                                                onchange="change_play_cam(this, 'videoElement')">
                                                            <?php if ($camera_in) {
                                                                foreach ($camera_in as $camera) {
                                                                    ?>
                                                                    <option value="<?= $camera->get_link_play() ?>"><?= $camera->title ?></option>

                                                                    <?php
                                                                }
                                                            } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <video width="100%" id="videoElement"></video>
                                            <canvas style="display: none" id="canvas_videoElement"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Biển số xe</label>
                                                <?= Html::textInput('car_number', $model->car_number, [
                                                    'autocomplete' => 'off',
                                                    'class' => 'form-control',
                                                    'readonly' => true,
                                                    'autofocus' => true,
                                                    'style' => 'text-transform:uppercase'
                                                ]) ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Ghi chú</label>
                                                <?= Html::textInput('note', $model->note, [
                                                    'autocomplete' => 'off',
                                                    'class' => 'form-control'
                                                ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header card-header-info text-center">
                                                    <h6 class="card-title">Ảnh biển số trước</h6>
                                                </div>
                                                <div class="card-body text-center">
                                                    <div class="form-group">
                                                        <input type="hidden" id="image_front" name="image_front"
                                                               value="<?= $model->image_front ?>"/>
                                                        <img id="img_snap_front" class="image_preview_snap"
                                                             src="<?= $model->image_front ?>"/>
                                                    </div>
                                                    <button onclick="snap_video('videoElement','img_snap_front','image_front')"
                                                            class="btn-capture btn btn-info btn-round" type="button">
                                                        Chụp
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header card-header-primary text-right">
                                                    <h6 class="card-title text-center">Ảnh chụp xe</h6>
                                                </div>
                                                <div class="card-body text-center">
                                                    <div class="form-group">
                                                        <input type="hidden" id="image_car" name="image_car"
                                                               value="<?= $model->image_car ?>"/>
                                                        <img id="img_snap_right" class="image_preview_snap"
                                                             src="<?= $model->image_car ?>"/>
                                                    </div>
                                                    <button onclick="snap_video('videoElement','img_snap_right','image_car')"
                                                            class="btn-capture btn btn-primary btn-round" type="button">
                                                        Chụp
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <a href="<?= route_to('bv_list_cars') ?>" class="btn btn-round">Huỷ</a>
                                            <button style="margin-left:15px" class="btn btn-success btn-round"
                                                    type="submit">Lưu
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($validator): ?>
                            <div class="alert alert-danger" style="margin-top: 32px;">
                                <ul style="margin: 0; padding-left: 16px;">
                                    <?php foreach ($validator->getErrors() as $error): ?>
                                        <li><?= Html::decode($error) ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
            </div>
        </div>
    </div>