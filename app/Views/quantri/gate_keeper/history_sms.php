<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Lịch sử tin nhắn';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-icon">
                    <i class="material-icons">search</i>
                </div>
            </div>
            <div class="card-body ">
                <form action="<?=route_to('bv_history_sms')?>" method="GET">
                    <div style="margin-bottom: 10px" class="row">
                        <div class="col-md-6">
                            <input placeholder="Biển số xe" type="text" name="vehicle_number" autocomplete="off" class="form-control" autofocus=""
                                   value="<?=$param_search['vehicle_number']?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class='input-group date' >
                                <input   type='text' name="sms_send_time" placeholder="Từ thời gian"
                                         class="form-control datetimepicker" autocomplete="off" value="<?=$param_search['sms_send_time']?>"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class='input-group date' >
                                <input type='text' name="sms_send_time" placeholder="Tới thời gian"
                                       value="<?=$param_search['sms_send_time']?>" autocomplete="off" class="form-control datetimepicker"/>
                            </div>
                    </div>

                        <div class="col-md-2">
                            <input type="submit" autocomplete="off" class="btn btn-info btn-round " value="Tìm kiếm">
                        </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?=$this->title?></h4>
        </div>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>STT</th>
		<th>Số lệnh</th>
                <th>Biển số xe</th>
                <th>Số điện thoại nhận</th>
                <th>Thời gian thành công</th>
                <th>Trạng thái</th>
            </tr>
            </thead>
            <tbody>
                <?php foreach ($models as $key => $model): ?>
                    <tr>
                        <td class="row-actions text-center"><?=++$key?></td>
                        <!-- <td><?=Html::decode($model->so_lenh)?></td> -->
			<td>
			     <?=$model->so_lenh?>
			</td>
                        <td>
                            <?=$model->vehicle_number?>
                        </td>
                        <td>
                            <?=$model->phone_receive?>
                        </td>
                        <td>
                            <?=$model->sms_send_time?>
                        </td>
                        <td>
                            <?=$model->sms_status?>
                        </td>

                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        <?=$pager->links('default', 'default_cms')?>
    </div>
</div>
