<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectCategoryModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Tạo mới xe';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-info flex-align">
                <div>
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>
            </div>
            <div class="card-body">
                <form action="<?='/quantri/car/create' . ($in_out_id?('/'.$in_out_id):'') ?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card" style="margin-bottom: 0px">
                                        <div class="card-header card-header-text card-header-info">
                                            <div class="card-text">
                                                <h6 class="card-title">Chọn camera</h6>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="">
                                                        <select class="form-control " data-style="btn btn-link"
                                                                id="select_camera_in"
                                                                onchange="change_play_cam(this, 'videoElement')">
                                                            <?php if ($camera_in) {
                                                                foreach ($camera_in as $camera) {
                                                                    ?>
                                                                    <option value="<?= $camera->get_link_play() ?>"><?= $camera->title ?></option>

                                                                    <?php
                                                                }
                                                            } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <video width="100%" id="videoElement"></video>
                                            <canvas style="display: none" id="canvas_videoElement"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Biển số xe</label>
                                                <?= Html::textInput('car_number', $car_number_detected?$car_number_detected:'', [
                                                    'autocomplete' => 'off',
                                                    'class' => 'form-control',
                                                    'autofocus' => true,
                                                    'style' => 'text-transform:uppercase'
                                                ]) ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="bmd-label-floating">Ghi chú</label>
                                                <?= Html::textInput('note', '', [
                                                    'autocomplete' => 'off',
                                                    'class' => 'form-control'
                                                ]) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header card-header-info text-center">
                                                    <h6 class="card-title">Ảnh biển số trước</h6>
                                                </div>
                                                <div class="card-body text-center">
                                                    <div class="form-group">
                                                        <input type="hidden" id="image_front" name="image_front"
                                                               value="<?= $model->image_front ?>"/>
                                                        <img id="img_snap_front" class="image_preview_snap"
                                                             src="<?= $model->image_front ? $model->image_front : '/images/empty.jpg' ?>"/>
                                                    </div>
                                                    <button onclick="snap_video('videoElement','img_snap_front','image_front')"
                                                            class="btn-capture btn btn-info btn-round" type="button">
                                                        Chụp
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header card-header-primary text-right">
                                                    <h6 class="card-title text-center">Ảnh chụp xe</h6>
                                                </div>
                                                <div class="card-body text-center">
                                                    <div class="form-group">
                                                        <input type="hidden" id="image_car" name="image_car"
                                                               value="<?= $model->image_car ?>"/>
                                                        <img id="img_snap_right" class="image_preview_snap"
                                                             src="<?= $model->image_car ? $model->image_car : '/images/empty.jpg' ?>"/>
                                                    </div>
                                                    <button onclick="snap_video('videoElement','img_snap_right','image_car')"
                                                            class="btn-capture btn btn-primary btn-round" type="button">
                                                        Chụp
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <a href="<?= route_to('bv_list_cars') ?>" class="btn btn-round">Huỷ</a>
                                            <button style="margin-left:15px" class="btn btn-success btn-round"
                                                    type="submit">Lưu
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($validator): ?>
                            <div class="alert alert-danger" style="margin-top: 32px;">
                                <ul style="margin: 0; padding-left: 16px;">
                                    <?php foreach ($validator->getErrors() as $error): ?>
                                        <li><?= Html::decode($error) ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
            </div>
        </div>
    </div>

    <script>
        window.onload = function () {
            setTimeout(function(){
                if($('#videoElement').attr('src')){
                    snap_video('videoElement','img_snap_front','image_front');
                    snap_video('videoElement','img_snap_right','image_car');
                    var _data = {
                        image_front: $('#image_front').val(),
                    }
                    $.post('/Admin/GateKeeper/bv_detect_car_number', _data, function (res) {
                        var result = JSON.parse(res);
                        if(result){
                            if(!$('input[name="car_number"]').val()){
                                $('input[name="car_number"]').val(result);
                                $('input[name="car_number"]').focus();
                            }
                        }
                    });
                }

            }, 2000);

        };
    </script>