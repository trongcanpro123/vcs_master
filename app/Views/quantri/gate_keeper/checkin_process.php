<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Kiểm soát vào ra';
?>
<div class="row " id="div_check_in">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-text">
                    <h6 class="card-title">Kiểm tra xe vào</h6>
                </div>
            </div>
            <div class="card-body">
                <div class="row mg-bot-30">
                    <div class="col-md-4 text-right">
                        <img height="60px" src="/images/qr_code_logo.jpg">
                    </div>
                    <div class="col-md-4">
                        <input style="font-size:20px " type="text" class="form-control" id="qr_value" name="qr_value"
                               placeholder="Quét mã QR" autofocus="autofocus"
                               onkeyup="qr_value_key_code(event,<?= $model->id  ?>)" autocomplete="off">
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row mg-bot-30">
                    <div class="col-md-12">
                        <?php
                        $icon_ok = '<button class="btn btn-success btn-sm btn-round" title="Khớp">
                                            <i class="material-icons">done</i>
                                            </button>';

                        $icon_not_ok = '<button class="btn btn-danger btn-sm btn-round" title="Không khớp">
                                            <i class="material-icons">help_center</i>
                                            </button>';
                        $btn_add_new_car = '<button onclick="process_register_car(' . $model->id . ',\''. $model->car_number_detected .'\')" class="btn btn-success btn-sm btn-round" title="Đăng ký xe">
                                            <i class="material-icons">add_box</i>
                                            </button>';

                        $btn_add_new_driver = '<button onclick="process_register_driver('. $model->id .')" class="btn btn-success btn-sm btn-round" title="Đăng ký tài xế">
                                            <i class="material-icons">add_box</i>
                                            </button>';
                        ?>
                        <div class="table-responsive">
                            <table id="table_check_in" class="table table-shopping">
                                <thead>
                                <tr>
                                    <th class="text-center" width="80px"></th>
                                    <th class="text-center " width="250px"><h6>Theo Lệnh</h6></th>
                                    <th class="text-center"><h6>Ảnh chụp quét</h6></th>
                                    <th class="text-center"><h6>Nhận diện</h6></th>
                                    <th class="text-center"><h6>Kết quả</h6></th>
                                    <th class="text-center"><h6>Thủ công</h6></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <h6>Biển số</h6>

                                    </td>
                                    <td class="text-center">
                                        <div>
                                           <?= $model->is_exist_car_number() ? '':$btn_add_new_car ?>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <img class="img-previe-table"
                                             src="<?= $model->car_number_image_detected ? $model->car_number_image_detected : '/images/empty.jpg' ?>"
                                             rel="nofollow" alt="...">
                                    </td>

                                    <td class="text-center td-name">
                                        <h3 style="font-weight: bold" id="h4_car_number_detected"><?= $model->car_number_detected ?></h3>

                                    </td>
                                    <td id="check_car_number" class="text-center">
                                        <?= $model->is_exist_car_number() ? $icon_ok : $icon_not_ok ?>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <?php $disabled = '';
                                                $checked = '';


                                                if ($model->checkin_car_number_hand || $model->checkin_car_number_note) {
                                                    $checked = 'checked';
                                                }
                                                ?>
                                                <input id="check_xu_ly_tay_bien_so"
                                                       class="form-check-input"  <?= $checked ?>
                                                       type="checkbox"
                                                       onchange="show_car_number_note('<?= $model->car_number_detected ?>')">
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>

                                                <input class="hidden" type="text" id="checkin_car_number_hand"
                                                       value="<?= $model->checkin_car_number_hand ?>">
                                                <input class="hidden" type="text" id="checkin_car_number_note"
                                                       value="<?= $model->checkin_car_number_note ?>">
                                            </label>
                                        </div>
                                    </td>


                                </tr>

                                <tr>
                                    <td>
                                        <h6>Tài xế</h6>
                                    </td>
                                    <td class="text-center">
                                        <div id="smo_driver_name">

                                            <?= $model->driver_name_detected? '<h3 style="font-weight: bold"> ' . $model->get_driver_name().'</h3>': $btn_add_new_driver  ?>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <img class="img-previe-driver-ai-table "
                                             src="<?= $model->driver_image_detected ? $model->driver_image_detected : '/images/empty.jpg' ?>"
                                             rel="nofollow" alt="...">
                                    </td>

                                    <td class="text-center td-name">
                                        <?php $anh_nhan_dien = $model->get_image_front_driver(); ?>
                                        <img class="img-previe-table"
                                             src="<?= $anh_nhan_dien ? $anh_nhan_dien : '/images/empty.jpg' ?>"
                                             rel="nofollow" alt="...">
                                    </td>
                                    <td id="check_driver" class="text-center">
                                        <?= $model->driver_name_detected ? $icon_ok : $icon_not_ok ?>
                                    </td>
                                    <td class="text-center">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <?php $disabled = 'disabled';
                                                if (!$model->driver_name_detected) {
                                                    $disabled = '';
                                                }

                                                $checked = '';
                                                if ($model->checkin_driver_hand || $model->checkin_driver_note) {
                                                    $checked = 'checked';
                                                }
                                                ?>
                                                <input id="check_xu_ly_tay_tai_xe"
                                                       class="form-check-input" <?= $disabled ?>  <?= $checked ?>
                                                       type="checkbox"
                                                       onchange="show_driver_note('<?= $model->driver_name_detected ?>')">
                                                <span class="form-check-sign">
                                                        <span class="check"></span>
                                                 </span>

                                                <input class="hidden" type="text" id="checkin_driver_hand"
                                                       name="checkin_driver_hand"
                                                       value="<?= $model->checkin_driver_hand ?>">
                                                <input class="hidden" type="text" id="checkin_driver_note"
                                                       name="checkin_driver_note"
                                                       value="<?= $model->checkin_driver_note ?>">
                                            </label>
                                        </div>
                                    </td>


                                </tr>
                                <?php
                                if ($list_smo_info) {
                                    foreach ($list_smo_info as $n => $smo_info) {
                                        ?>
                                        <tr tr_qr_code="<?= $smo_info->do_sap ?>">
                                            <td><h6><?php if ($n == 0) echo 'Mã QR'; ?></h6></td>
                                            <td class="text-center td-name">
                                                <button onclick="show_order_detai('<?= $smo_info->do_sap ?>')"
                                                        class="btn btn-social btn-fill btn-info btn-round">
                                                    <i class="material-icons">qr_code</i> <?= $smo_info->do_sap ?>
                                                </button>
                                            </td>

                                            <td class="text-center">
                                                <div>
                                                    <h4 h4_driver_name><?= trim($smo_info->oic_pbatch . ' ' . $smo_info->oic_ptrip) ?></h4>
                                                </div>
                                            </td>
                                            <td class="text-center">
                                                <div>
                                                    <h4 h4_car_number
                                                        data-qr_code="<?= $smo_info->do_sap ?>"><?= $smo_info->vehicle_code ?></h4>
                                                </div>
                                            </td>

                                            <td id="check_car_number" class="text-center">
                                                <button class="btn btn-success btn-sm btn-round" title="Khớp">
                                                    <i class="material-icons">done</i>
                                                </button>
                                            </td>
                                            <td class="text-center">
                                                <button onclick="delete_smo_info('<?= $smo_info->do_sap ?>',<?= $model->id ?>)"
                                                        class="btn btn-danger btn-sm btn-round  title=" Xóa
                                                "">
                                                <i class="material-icons">delete_forever</i>
                                                </button>
                                            </td>


                                        </tr>

                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

                <div class="row">

                    <div class="col-md-3">
                        <button type="button"  onclick="process_checkin_cancel(<?= $model->id ?>)" class="btn btn-dark btn-block btn-lg">Hủy
                        </button>

                    </div>
                    <div class="col-md-3">
                        <button type="button" onclick="process_checkin_reject(<?= $model->id ?>)" class="btn btn-danger btn-block btn-lg">
                            Không cho vào
                        </button>

                    </div>
                    <div class="col-md-3">
                        <button type="button" onclick="process_checkin_not_set_order(<?= $model->id ?>)"
                                class="btn btn-warning btn-block btn-lg">Không cấp số
                        </button>

                    </div>
                    <div class="col-md-3">
                        <button type="button" onclick="process_checkin_set_order(<?= $model->id ?>,'<?= $model->car_number_detected ?>','<?= $model->driver_name_detected; ?>','<?= $model->is_exist_car_number() ?>')" class="btn btn-success btn-block btn-lg">Cấp số</button>

                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="col-md-1"></div>

</div>

<div id="qr_info_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Thông tin đơn hàng</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-left "><h6>Mã mặt hàng</h6></th>
                        <th class="text-left"><h6>Tên mặt hàng</h6></th>
                        <th class="text-center"><h6>Số lượng</h6></th>
                    </tr>
                    </thead>
                    <tbody id="tbody_smo_order_detail">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>

<div id="car_number_hand_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Xử lý tay biển số xe</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" onchange="process_customer_type()" type="radio" name="customer_type" value="smo"
                                      <?= $model->customer_type == 'smo' ? 'checked': '' ?> >Khách SMO
                                <span class="circle">
                              <span class="check"></span>
                            </span>
                            </label>

                            <label class="form-check-label">
                                <input  class="form-check-input" onchange="process_customer_type()" type="radio" name="customer_type" value="not_smo"
                                    <?= $model->customer_type == 'not_smo' ? 'checked': '' ?>  > Khách Ngoài SMO
                                <span class="circle">
                              <span class="check"></span>
                            </span>
                            </label>



                        </div>
                    </div>
                    <div id="div_for_khach_le" class="col-sm-12 <?= $model->customer_type == 'not_smo' ? '': 'hidden' ?>  ">
                        <div class="form-group bmd-form-group">
                            <input id="form_car_number_hand" type="text" autocomplete="off" placeholder="Biển số xe" value="<?= $model->car_number_detected ?>"
                                   style="text-transform:uppercase" class="form-control">
                            <span class="bmd-help">Nhập biển số xe trong trường hợp khách lẻ không có QR code</span>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group bmd-form-group">
                            <input id="form_car_number_note" type="text" autocomplete="off" placeholder="Lý do/Ghi chú"
                                   required class="form-control">
                            <span class="bmd-help">Nhập lý do/ghi chú</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                <button type="button" onclick="update_car_number_note(<?= $model->id ?>)" class="btn btn-success">Lưu
                    lại
                </button>

            </div>
        </div>
    </div>
</div>

<div id="driver_hand_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Xử lý tay tài xế</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div id="div_for_khach_le" class="col-sm-12">
                        <div class="form-group bmd-form-group">
                            <input id="form_driver_hand" type="text" autocomplete="off" placeholder="Tên tài xế"
                                   class="form-control">
                            <span class="bmd-help">Nhập tài xế trong trường hợp khách lẻ</span>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group bmd-form-group">
                            <input id="form_driver_note" type="text" autocomplete="off" placeholder="Lý do/Ghi chú"
                                   required class="form-control">
                            <span class="bmd-help">Nhập lý do/ghi chú</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                <button type="button" onclick="update_driver_note(<?= $model->id ?>)" class="btn btn-success">Lưu lại
                </button>

            </div>
        </div>
    </div>
</div>

<script>
    window.onload = function () {
        $('#qr_value').focus();
        $('#qr_value').select();
        var list_qr_code = <?= $list_qr_code ?>;
        localStorage.setItem("list_qr_code", JSON.stringify(list_qr_code));

        if(<?= $model->checkin_order > 0 ? '1':'0' ?>){
            $('#div_check_in').find('input, textarea, button, select').attr('disabled','disabled');
        }
    };
</script>