<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectCategoryModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Số thứ tự';
?>
<!doctype html>
<html>
<head>

    <title><?= $this->title ?></title>
    <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <style>
        html {margin:0;padding:0;}
        /*@media print {*/
        /*    body {*/
        /*        width: 300px*/
        /*        height: 600px*/
        /*    }*/
        /*    !* etc *!*/
        /*}*/
        @media print {
            #printarea * {
                display:block;
            }
            .not_print{
                display: none;
                margin: 0px;
            }
        }
        body{
            width: 300px;
            height: 300px;
            font-family: Arial;
            margin: 0;
            padding: 0;
        }
        @page {
            margin: 0mm 1mm 1mm 1mm;
        }
    </style>
</head>
<body>
<button class="not_print" style="height: 30px" onclick="window.print();">In STT</button>
<table width="100%">
    <tbody>

    <tr>
       <td colspan="2" style="text-align: center"> <image style="margin: 0; height: 70px" src="/images/logo_petro.png"  alt=""></td>
    </tr>
    <tr>
        <td colspan="2" style="vertical-align:bottom; text-align: center">
            <div style="font-weight: bold; font-size: 32px; margin-bottom: 10px; margin-top: 5px">Số: <?= $stt?></div>
        </td>

    </tr>
    <tr>
        <td style="width: 30%">
            <div style="float: left;font-weight: bold; font-size: 20px;margin-bottom: 10px">Biển số:</div>
        </td>
        <td>
            <div style="float: left;font-weight: bold; font-size: 20px;margin-bottom: 10px"><?= $carNumber?></div>
        </td>
    </tr>
    <tr>
        <td  style="width: 30%">
            <div style="float: left;font-weight: bold; font-size: 20px;margin-bottom: 10px">Giờ ĐK: </div>
        </td>
        <td>
            <div style="float: left;font-weight: bold; font-size: 20px;margin-bottom: 10px"><?= date('d-m-Y H:i:s', strtotime($checkin_time))?></div>
        </td>
    </tr>
    <tr>
        <td  style="width: 50%">
            <div style="float: left;font-weight: bold; font-size: 20px;margin-bottom: 10px">Ngày hiệu lực: </div>
        </td>
        <td>
            <div style="float: left;font-weight: bold; font-size: 20px;margin-bottom: 10px"><?= date('d-m-Y', strtotime($checkin_out))?></div>
        </td>
    </tr>
    </tbody>

</body>
<script>
    window.onbeforeunload = function(){
        // set warning message
        window.opener.location.href="/quantri/bao-ve-kiem-soat-vao-ra";
    };
</script>
</html>