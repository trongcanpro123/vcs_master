<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Danh sách xe';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-icon">
                    <i class="material-icons">search</i>
                </div>
            </div>
            <div class="card-body ">
                <form action="<?= route_to('bv_list_cars') ?>" method="GET">
                    <div class="row">
                        <div class="col-md-4">
                            <input placeholder="Biển số xe" type="text" name="car_number" autocomplete="off"
                                   class="form-control" autofocus=""
                                   value="<?= $param_search['car_number'] ?>">
                        </div>
                        <div class="col-md-4">
                            <input type="submit" autocomplete="off" class="btn btn-info btn-round btn-sm"
                                   value="Tìm kiếm">
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
        </div>
        <a href="<?= route_to('admin_car_create') ?>" class="btn btn-warning btn-round btn-sm">Thêm mới</a>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>STT</th>
                <th>Biển số xe</th>
                <th style="text-align:center">Ảnh biển số trước</th>
                <th style="text-align:center">Ảnh chụp xe</th>
                <th style="width: 120px">Hành động</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!$models || empty($models)): ?>
                <tr>
                    <td colspan="100">
                        <div class="empty-block">
                            <img src="/images/no-content.jpg" alt="No content"/>
                            <h4>Không có nội dung</h4>
                            <a class="btn btn-info btn-round"
                               href="<?= route_to('admin_car_create') ?>">Thêm</a>
                        </div>
                    </td>
                </tr>
            <?php else: ?>
                <?php foreach ($models as $key => $model): ?>
                    <tr>
                        <td class="row-actions text-center"><?= ++$key ?></td>
                        <td><?= Html::decode($model->car_number) ?></td>
                        <td style="text-align:center">
                            <img class="image_preview_snap_table" src="<?= $model->image_front ?>"/>
                        </td>
                        <td style="text-align:center">
                            <img class="image_preview_snap_table" src="<?= $model->image_car ?>"/>
                        </td>
                        <td class="row-actions">
                            <?= Html::a('<i class="material-icons">edit</i>', ['admin_car_update', $model->getPrimaryKey()], [
                                'class' => ['btn', 'btn-info', 'btn-just-icon', 'btn-sm'],
                                'title' => 'Sửa'
                            ]) ?>
                            <a href="<?= route_to('admin_car_delete', $model->getPrimaryKey()) ?>"
                               class="btn btn-danger btn-just-icon btn-sm" data-method="post"
                               data-prompt="Bạn có chắc sẽ xoá đi mục này?">
                                <i class="material-icons">delete</i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif ?>
            </tbody>
        </table>
        <?= $pager->links('default', 'default_cms') ?>
    </div>
</div>
