<?php
use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\CarInOutHistoryModel $model
 * @var \CodeIgniter\Pager\Pager $pager
 **/


$this->title = 'Kiểm soát vào ra';
?>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-text">
                    <h6 class="card-title">Ảnh chụp xe ra</h6>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <img width="100%" src="<?= $checkout_image; ?>">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-text">
                    <h6 class="card-title">Thông Tin</h6>
                </div>
            </div>
            <div class="card-body">
                <?php if($model){
                    ?>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 ><span style="width: 180px; display: inline-block">Họ tên tài xế:</span> <b><?= $model->driver_name ?></b></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4><span style="width: 180px; display: inline-block">Biển số xe:</span> <b>
                                            <?php
                                                if($model && $model->car_number){
                                                    echo $model->car_number;
                                                }else{
                                                    echo $checkout_ai_car_number;
                                                }
                                                ?>
                                            </b></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <?php
                                        $btn_ok = '<button class="btn btn-success btn-sm btn-round" title="Đã đủ hàng">
                                            Đã đủ hàng
                                            </button>';
                                        $btn_not_ok = '<button class="btn btn-danger btn-sm btn-round" title="Chưa đủ hàng">
                                            Chưa đủ hàng
                                            </button>';
                                        $btn_null = '<button class="btn btn-danger btn-sm btn-round" title="Chưa đủ hàng">
                                            Không tồn tại lịch sử xe vào
                                            </button>'
                                        ?>
                                        <h4><span style="width: 180px; display: inline-block">Trạng thái lấy hàng: </span>
                                            <?php
                                                if($model){
                                                    echo $model->checkout_order_status?$btn_ok: $btn_not_ok;
                                                }else{
                                                    echo $btn_null;
                                                }
                                            ?>
                                        </h4>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <div class="row text-center">
                        <div class="col-md-12">
                            <br>
                            <h8>Nếu thông tin biển số không chính xác, vui lòng nhấn nút kiểm tra lại để cung cấp thông tin chính xác</h8>
                        </div>
                    </div>
                <?php
                }else {
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                                if($checkout_ai_car_number){
                                    ?>
                                    <h4>Biển số xe: <?php echo $checkout_ai_car_number; ?></h4>

                                    <?php
                                }
                            ?>

                            <h4>Hệ thống không xác minh được thông tin xe, vui lòng cung cấp thông tin ở nút kiểm tra lại</h4>

                        </div>
                    </div>
                <?php
                } ?>
                <div class="row">
                    <div class="col-md-4">
                        <button type="button" onclick="process_checkout_reject(<?= $camera_id ?>,'<?= $model->id ?>')" class="btn btn-dark btn-block btn-lg">Không cho ra
                        </button>
                    </div>
                    <div class="col-md-4">
                        <button type="button" onclick="show_re_check_car_number()" class="btn btn-warning btn-block btn-lg">Kiểm tra lại
                        </button>
                    </div>
                    <div class="col-md-4">
                        <button type="button" onclick="process_checkout_allow('<?=$model->car_number?>',<?=$model->area_id?>,<?= $camera_id ?>,<?= $model?$model->checkout_order_status : 0 ?>,'<?= $model->id ?>')" class="btn btn-success btn-block btn-lg">Cho ra
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="car_number_hand_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Kiểm tra lại thông tin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div  class="col-sm-12 ">
                    <div class="form-group bmd-form-group">
                        <input id="form_car_number_hand" type="text" autocomplete="off" placeholder="Biển số xe"
                               style="text-transform:uppercase" class="form-control">
                        <span class="bmd-help">Nhập biển số xe trong trường hợp nhận diện sai thông tin</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                <button type="button" onclick="re_check_car_number_info(<?= $camera_id ?>)" class="btn btn-success">Kiểm tra
                </button>
            </div>
        </div>
    </div>
</div>


<div id="form_checkout_by_hand_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Xử lý cho ra</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div  class="col-sm-12 ">
                    <div class="form-group bmd-form-group">
                        <input id="form_checkout_note_hand" type="text" autocomplete="off" placeholder="Lý do/Ghi chú" class="form-control" required>
                        <span class="bmd-help">Xe ra vẫn chưa đủ điều kiện, vui lòng nhập lý do hoặc ghi chú</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                <button type="button" onclick="checkout_by_hand('<?=$model->car_number?>',<?=$model->area_id?>, <?= $camera_id ?>,<?= $model->id ?>)" class="btn btn-success">Cho ra
                </button>
            </div>
        </div>
    </div>
</div>
