<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Trợ giúp';
?>

<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
            <p style="color: #FFFFFF" class="card-category">Dưới đây là các hướng dẫn dành cho bảo vệ</p>
        </div>
    </div>
    <div class="card-body table-responsive">
        <div class="row">
            <div class="col-md-12">
                <h3>Hướng dẫn xế vào xe lấy hàng.</h3>
                <div style="padding-left: 25px">
                    <p>Bước 1: Đỗ xe đúng chỗ, đúng nơi quy định.</p>
                    <p>Bước 2: Quét danh sách mã lệnh (QR) cần lấy hàng.</p>
                    <p>Bước 3: Đứng đúng vị trí, hướng mặt về camera để chụp hình khuôn mặt.</p>
                    <p>Bước 4: Lấy số thứ tự vào kho.</p>
                </div>
            </div>
            <div class="col-md-12">
                <h3>Hướng dẫn xế ra khỏi kho.</h3>
                <div style="padding-left: 25px">
                    <p>Bước 1: Lấy xong hàng hóa.</p>
                    <p>Bước 2: Đỗ xe đúng nơi, đúng quy định.</p>
                    <p>Nếu đủ điều kiện sẽ được ra.</p>
                </div>
            </div>
        </div>
    </div>
</div>
