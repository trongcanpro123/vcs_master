<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Lịch sử vào ra';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-icon">
                    <i class="material-icons">search</i>
                </div>
            </div>
            <div class="card-body ">
                <form action="<?= route_to('bv_in_out_history') ?>" method="GET">
                    <div style="margin-bottom: 10px" class="row">
                        <div class="col-md-6">
                            <input placeholder="Tên tài xế" type="text" name="driver_name" autocomplete="off" class="form-control" autofocus=""
                                   value="<?= $param_search['driver_name'] ?>">
                        </div>
                        <div class="col-md-6">
                            <input placeholder="Biển số xe" type="text" name="car_number" autocomplete="off" class="form-control" autofocus=""
                                   value="<?= $param_search['car_number'] ?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class='input-group date' >
                                <input   type='text' name="start_time" placeholder="Từ thời gian"
                                         class="form-control datetimepicker" autocomplete="off" value="<?= $param_search['start_time'] ?>"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class='input-group date' >
                                <input type='text' name="end_time" placeholder="Tới thời gian"
                                       value="<?=  $param_search['end_time'] ?>" autocomplete="off" class="form-control datetimepicker"/>
                            </div>
                    </div>
                        <div class="col-md-2">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" name="checkin_order" type="checkbox" <?= $param_search['checkin_order'] ?>>
                                    Không cấp số
                                    <span class="form-check-sign"><span class="check"></span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <input type="submit" autocomplete="off" class="btn btn-info btn-round " value="Tìm kiếm">
                        </div>
            </div>
            </form>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
        </div>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>STT</th>
                <th>Tài xế</th>
                <th>Biển số</th>
                <th>Thời gian vào</th>
                <th>Thời gian ra</th>
                <th>Ghi chú</th>
                <th>Hành động</th>

            </tr>
            </thead>
            <tbody>
            <?php if (!$models || empty($models)): ?>
                <tr>
                    <td colspan="100">
                        <div class="empty-block">
                            <img src="/images/no-content.jpg" alt="No content"/>
                            <h4>Không có nội dung</h4>
                            <a class="btn btn-info btn-round"
                               href="<?= route_to('admin_driver_create') ?>">Thêm</a>
                        </div>
                    </td>
                </tr>
            <?php else: ?>
                <?php foreach ($models as $key => $model): ?>
                    <tr>
                        <td class="row-actions text-center"><?= ++$key ?></td>
                        <td><?= Html::decode($model->driver_name) ?></td>
                        <td>
                            <?= $model->car_number ?>
                        </td>
                        <td>
                            <?= $model->checkin_time? date('d-m-Y H:i:s', strtotime($model->checkin_time)): '' ?>
                        </td>
                        <td>
                            <?= $model->checkout_time? date('d-m-Y H:i:s', strtotime($model->checkout_time)):'' ?>
                        </td>
                        <td>
                            <?php
                            $note = '';
                            if( $model->checkin_order <0 ){
                                $note = 'Không cho vào';

                            }
                            if( $model->checkin_order == 0 ){
                                $note = 'Không cấp số';
                            }
                            if( $model->checkin_order > 0 ){
                                $note = 'Đã cấp số: ' . $model->checkin_order;
                            }
                            ?>
                            <?= $note ?>
                        </td>
                        <td class="row-actions">
                            <?php if($model->checkin_order == 0): ?>
                                <a href="/quantri/xu-ly-check-in/<?= $model->id ?>" type="button" class="btn btn-info btn-round btn-sm" >
                                   Tiếp tục
                                </a>
                            <?php endif; ?>
                            <?php if($model->checkin_order > 0): ?>
                                <a href="#" onclick="re_print_order(<?= $model->id ?>);" title="In STT"  type="button" class="btn btn-success btn-round btn-sm">
                                    <i class="material-icons">print</i>
                                </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif ?>
            </tbody>
        </table>
        <?= $pager->links('default', 'default_cms') ?>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="comfirm-checkin" tabindex="-1" role="dialog" aria-labelledby="comfirm-checkin" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="comfirm-checkin">Cấp số cho tài xế</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-assign-number" class="btn btn-primary ">Cấp số</button>
                <button type="button" id="btn-print-number" class="btn btn-primary">In số</button>
            </div>
        </div>
    </div>
</div>
