<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Báo cáo chi tiết xe';
?>



<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-text card-header-info">
                        <div class="card-icon">
                            <i class="material-icons">search</i>
                        </div>

                    </div>
                    <div class="card-body ">
                        <form action="<?php base_url() . '/Administrator/in_out_detail'; ?>" method="get">
                            <div class="row">
                                <div class="col-md-5">
                                    <select name="area_id" class="form-control mr-sm-2 " placeholder="Chọn kho">

                                        <?php
                                        $current_user = (\App\Models\AdministratorModel::findIdentity());
                                        if($current_user->type_user == 'lanh_dao'){
                                            ?>
                                            <option value="0" >Tất cả</option>

                                            <?php
                                        }
                                        if ($area_models) {
                                            foreach ($area_models as $area) {
                                                if($current_user->type_user == 'lanh_dao' || $current_user->area_id == $area->id){
                                                    ?>
                                                    <option value="<?= $area->id ?>" <?php if ($area->id == $param_search['area_id']) {
                                                        echo 'selected';
                                                    } ?> ><?= $area->name ?></option>
                                                <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </select>

                                </div>
                                <div class="col-md-3">
                                    <div class='input-group date'>
                                        <input type='text' id="date_in_car" name="select_date" placeholder="Chọn ngày"
                                               class="form-control datepicker" autocomplete="off"
                                               value="<?= $param_search['select_date'] ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-4 text-center">
                                    <button class="btn btn-info btn-round" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card" style="margin-top: 0px">
                    <div class="card-header  card-header-text card-header-info flex-align">
                        <div class="card-text">
                            <h4 class="card-title">Số liệu</h4>
                        </div>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Giờ</th>
                                <th>SL xe vào</th>
                                <th>SL xe ra</th>
                                <th>SL xe không hợp lệ</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (!$report_result || empty($report_result) || !$report_result['so_lieu']): ?>
                                <tr>
                                    <td colspan="100">
                                        <div class="empty-block">
                                            <img src="/images/no-content.jpg" alt="No content"/>
                                            <h4>Không có nội dung</h4>

                                        </div>
                                    </td>
                                </tr>
                            <?php else: ?>
                                <?php foreach ($report_result['so_lieu'] as $n => $model): ?>
                                    <tr>
                                        <td><?= $model['stt'] ?></td>
                                        <td><?= $model['gio'] ?></td>
                                        <td><?= $model['slxv']  ?></td>
                                        <td><?= $model['slxr'] ?></td>
                                        <td><?= $model['slxkhl'] ?></td>

                                    </tr>
                                <?php endforeach; ?>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-icon card-header-info">
                <div class="card-icon">
                    <i class="material-icons">timeline</i>
                </div>
                <h4 class="card-title">Biểu đồ
                </h4>
            </div>
            <div class="card-body">
                <canvas id="report_bc_xe_chi_tiet" width="300px" height="200px"  style="display: block; box-sizing: border-box; width: 300px; height: 200px; "></canvas>
            </div>
        </div>

    </div>

</div>
<script>
    window.onload = function() {

        var ctx = document.getElementById("report_bc_xe_chi_tiet").getContext('2d');
        // const labels  = [
        //     'January',
        //     'February',
        //     'March',
        //     'April',
        //     'May',
        //     'June',
        //     'July'
        // ];
        // const data = {
        //     labels: labels,
        //     datasets: [{
        //         label: 'My First Dataset',
        //         data: [65, 59, 80, 81, 56, 55, 40],
        //         fill: false,
        //         borderColor: 'rgb(75, 192, 192)',
        //         tension: 0.1
        //     }]
        // };
        const data = <?=  $report_result['bieu_do'] ?>;
        const config = {
            type: 'line',
            data: data,
            options: {
                responsive: true, maintainAspectRatio: true,
                scales: {
                    yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: true,
                            stepSize: 1,
                        }
                    }],
                },
            }
        };
        var chart_bc_xe_chi_tiet = new Chart(ctx, config);

        
        jQuery('#date_in_car').datetimepicker({
            format: 'd-m-Y',
            timepicker:false,
        });
    };
</script>