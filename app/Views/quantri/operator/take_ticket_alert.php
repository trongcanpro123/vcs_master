<?php

use App\Helpers\Assets\AdminAsset;
use App\Helpers\Widgets\AdminAppBar;
use App\Helpers\Widgets\AdminNav;
use App\Helpers\SessionHelper;
use App\Models\SettingsModel;
use App\Helpers\Html;

/**
 * @var $this \App\Libraries\BaseView
 * @var string $title
 * @var string $content
 * @var \CodeIgniter\HTTP\Request $request
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
//try{
AdminAsset::register($this);

//}catch (\Exception $ex){
//    var_dump($ex);
//}
//die;
$this->title = 'Thông Báo Lấy Ticket';
?>
<!doctype html>
<html>
<head>

    <title><?= $this->title ?></title>
    <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <?php $this->head() ?>
    <style>
        body {
            background-color: #4792e8 !important;
            color: white !important;
        }

        .theme_alert {
            background-color: #4792e8 !important;
            color: white !important;
        }

        table thead th {
            background-color: #ff9b63 !important;
            color: white !important;
            font-weight: bold !important;
            font-size: 40px !important;
            border: solid 1px white;
        }

        table tbody tr td {
            background-color: #4792e8 !important;
            color: white !important;
            font-weight: bold !important;
            font-size: 30px !important;
            padding-top: 20px !important;
            padding-bottom: 20px !important;
        }

        table tr td {
            border: solid 1px white;
            border-color: white;
        }


    </style>
</head>
<body>
<div class="wrapper ">
    <div class="row">
        <div class="col-md-12">

            <div class="card theme_alert" style="margin-bottom: 0px">
                <div class="card-body text-center">
                    <?php
                    if ($list_alert && count($list_alert)) {
                        $alert_message = "Xin mời ";

                        foreach ($list_alert as $in_out) {
                            $alert_message .= 'Số: ' . $in_out->checkin_order . ' - ' . $in_out->driver_name . ' - ' . $in_out->car_number . ', ';
                        }
                        $alert_message .= 'vào lấy ticket';
                    }

                    ?>
                    <div class="main-logo-petro">
                        <img src="/images/logo_petro.png" alt="">
                    </div>
                    <div id="alert_message" style="text-transform:uppercase;  float: left;width: 90%"><h2
                                style="font-weight: bold"><?= $alert_message ?></h2></div>

                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card theme_alert" style="margin-top: 5px">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <table class="table table-striped theme_alert">
                            <thead>
                            <tr>
                                <th style="width: 10%"  border: solid 1px">Số thứ tự</th>
                                <th style="width: 20%"  border: solid 1px">Tài xế</th>
                                <th style="width: 20%"  border: solid 1px">Biển số xe</th>
                                <th style="width: 50%"  border: solid 1px">Ghi chú</th>
                            </tr>

                            </thead>
                            <tbody id="myTable">

                            <?php
                            if ($list_wait) {
                                foreach ($list_wait as $item) {
                                    ?>
                                    <tr>
                                        <td><?= $item->checkin_order ?></td>
                                        <td><?= $item->driver_name ?></td>
                                        <td><?= $item->car_number ?></td>
                                        <td><?= $item->note ?></td>
                                    </tr>

                                    <?php
                                }

                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                </div>


            </div>
        </div>
    </div>

</div>

<?php

$this->registerAssets();
?>
</body>

</html>
<script>
    var alert_message = undefined;
    window.onload = function() {
        $('#alert_message').bind("finished", function() {
            $('#alert_message').marquee('pause');
            reload_this_page();
        }).marquee({
            "duration": 20000,
            "pauseOnHover": true
        });

    };
    function reload_this_page(){
        location.reload();
    }
</script>