<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Thứ tự lấy hàng';
?>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-text card-header-info">
                        <div class="card-icon">
                            <i class="material-icons">search</i>
                        </div>

                    </div>
                    <div class="card-body ">
                        <form  method="get">
                            <div class="row">

                                <div class="col-md-2">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" id="check_all" name="check_all" type="checkbox"
                                                   <?php if($check_all){
                                                       echo ' checked ';
                                                   } ?>
                                                   onchange="submit();" >
                                            Hiển thị tất cả
                                            <span class="form-check-sign"><span class="check"></span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-10 text-center">
                                    <button class="btn btn-info btn-round" type="submit">Tải lại dữ liệu</button>
                                    <a style="color: white" target="_blank" href="/thong-bao-lay-hang/<?= $nhan_vien->area_id ?>/1"  class="btn btn-rose btn-round" >Xem màn hình 01</a>
                                    <a style="color: white" target="_blank"  href="/thong-bao-lay-hang/<?= $nhan_vien->area_id ?>/2"  class="btn btn-rose btn-round" >Xem màn hình 02</a>
                                    <a style="color: white" target="_blank" href="/thong-bao-lay-hang-am-thanh/<?= $nhan_vien->area_id ?>/1"  class="btn btn-primary btn-round" >Phát loa màn hình 01</a>
                                    <a style="color: white" target="_blank" href="/thong-bao-lay-hang-am-thanh/<?= $nhan_vien->area_id ?>/2"  class="btn btn-primary btn-round" >Phát loa màn hình 02</a>


                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-info flex-align card-header-text"  >
                        <div class="card-text" >
                            <h4 class="card-title">Thông tin</h4>
                        </div>
                    </div>
                    <div class="card-body table-responsive">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Tài xế</th>
                                <th>Biển số</th>
                                <th>Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody id="myTable">

                            <?php if ($list_in_out_take_item): ?>
                                <?php foreach ($list_in_out_take_item as $item): ?>
                                    <tr>
                                        <td><?= $item->driver_name ?></td>
                                        <td><?= $item->car_number ?></td>
                                        <td><?= $item->get_trang_thai_lay_hang() ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif ?>


                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script>
    window.onload = function () {
        setInterval(reload_this_page, 5*60*1000);
    };
    function reload_this_page(){
        location.reload();
    }
</script>
