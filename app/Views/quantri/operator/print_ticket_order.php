<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectCategoryModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Số thứ tự';
?>
<!doctype html>
<html>
<head>

    <title><?= $this->title ?></title>
    <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <style>
        html {margin:0;padding:0;}
        /*@media print {*/
        /*    body {*/
        /*        width: 300px*/
        /*        height: 600px*/
        /*    }*/
        /*    !* etc *!*/
        /*}*/
        @media print {
            #printarea * {
                display:block;
            }
            .not_print{
                display: none;
                margin: 0px;
            }
        }
        body{
            width: 595px;
            height: 842px;
            font-family: Arial;
            margin: 0;
            padding: 0;
        }
        @page {
            margin: 30mm 20mm 30mm 15mm;
        }
        table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
}
    </style>
</head>

<body>
<button class="not_print" style="height: 30px" onclick="window.print();">In Ticket</button>
<table width="100%">
    <tbody>
    <tr>
        <td colspan="2" style="text-align: left">Đơn vị: VP công ty XD Nghệ An</td>
        <td colspan="2" style="text-align: left">Số tích kê:<?= $number?>  </td>
    </tr>
    <tr>
       <td colspan="1" style="text-align: left"> <image style="margin: 0; height: 100px" src="/images/logo_petro.png"  alt=""></td>
       <td colspan="1" style="text-align: left;">TÍCH KÊ XUẤT HÀNG HOÁ NỘI BỘ</td>
    </tr>
    <tr>
        <td>
            <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 1px">Số phương tiện: <?= $model['MaPhuongTien']?></div>
        </td>
        <td>
            <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 1px">Người vận chuyển:<?= $model['NguoiVanChuyen']?></div>
        </td>
        <td>
            <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 1px">PT Bán:<?= $model['MaPhuongThucBan']?></div>
        </td>
    </tr>
    <tr>
        <td>
            <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 1px">Khách hàng:<?= $model['MaKhachHang']?></div>
        </td>
        <td>
            <div style="float: left;font-weight: normal; font-size: 12px;margin-bottom: 1px">Chuyến vận tải:<?= $model['MaVanChuyen']?></div>
        </td>
    </tr>
    <table class="table table-striped">
        <thead>
        <tr>
          <th>Số lệnh</th>
          <th>Mã tự động hoá</th>
          <th>Tên hàng hoá</th>
          <th>Mã bể</th>
          <th>Thứ tự ngăn</th>
          <th>Dung tích ngăn</th>
          <th>Mã số lượng</th>
          <th>Lượng xuất thực tế</th>
          <th>Nhiếu độ thực thế</th>
          <th>Khoảng cách so với tấm mức</th>
        </tr>
        </thead>
        <tbody id="myTable">

        <?php if ($model): ?>
            <?php foreach ($model as $item): ?>
                <tr>
                <td><?=$item['SoLenh']?></td>
          <td style="text-align:right"><?=$item['MaTuDongHoa']?></td>
          <td style="text-align:right"><?=$item['TenHangHoa']?></td>
          <td style="text-align:right"><?=$item['MaBeXuat']?></td>
          <td style="text-align:right"></td>
          <td style="text-align:right"></td>
          <td style="text-align:right"></td>
          <td style="text-align:right"></td>
          <td style="text-align:right"></td>
          <td style="text-align:right"></td>
                </tr>
            <?php endforeach; ?>
        <?php endif ?>
        </tbody>
    </table>
    </tbody>

</body>
<script>
    window.onbeforeunload = function(){
        // set warning message
        window.opener.location.href="/quantri/bao-ve-kiem-soat-vao-ra";
    };
</script>
</html>
