<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Thứ tự lấy ticket';
?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-text card-header-info">
                        <div class="card-icon">
                            <i class="material-icons">search</i>
                        </div>

                    </div>
                    <div class="card-body ">
                        <form method="get">
                            <div class="row">
                                <div class="col-md-3">
                                    <input name="driver_name" class="form-control" type="search"
                                           placeholder="Tên tài xế"
                                           aria-label="Search"
                                           value="<?= $param_search ? $param_search['driver_name'] : '' ?>">
                                </div>
                                <div class="col-md-3">
                                    <div class='input-group date'>
                                        <input type='text' name="car_number" placeholder="Biển số"
                                               class="form-control " autocomplete="off"
                                               value="<?= $param_search ? $param_search['car_number'] : '' ?>">
                                    </div>
                                </div>

                                <div class="col-md-6 text-center">
                                    <button class="btn btn-info btn-round" type="submit">Tìm kiếm</button>
                                    <a style="color: white" class="btn btn-rose btn-round" target="_blank"
                                       href="/thong-bao-nhan-ticket/<?= $area_id ?>">Màn hình thông báo</a>

                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-info flex-align">
                        <div>
                            <h4 class="card-title">Thứ tự lấy Ticket</h4>
                        </div>
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tài xế</th>
                                <th>Biển số</th>
                                <th>Thời gian vào</th>
				<th>Ghi chú</th>
                                <th class="text-center">Thực hiện</th>

                            </tr>
                            </thead>
                            <tbody id="myTable">

                            <?php
                            if ($models): ?>
                                <?php foreach ($models as $n => $model): ?>
                                    <tr>
                                        <td><?= $model->checkin_order ?></td>
                                        <td><?= $model->driver_name ?></td>
                                        <td><?= $model->car_number ?></td>

                                        <td><?= date('d-m-Y H:i:s', strtotime($model->checkin_time)) ?></td>
					<td><?= $model->note ?></td>
                                        <td class="row-actions text-right">
                                           
                                                <button id="process_alert_print_ticket_<?= $model->id ?>"
                                                    class="btn btn-round btn-success btn-sm"
                                                    onclick="print_order()" value="Print">
                                                In Ticket
                                            </button>
                                            <button id="process_alert_get_ticket_<?= $model->id ?>"
                                                    class="btn btn-round btn-success btn-sm"
                                                    onclick="process_take_ticket(<?= $model->id ?>)" value="Ticket">
                                                Mời vào
                                            </button>
                                                <!-- <a id="btn_alert_take_ticket_<?= $model->id ?>"
                                                   class="btn btn-round btn-info btn-sm"
                                                   href="/Admin/Operator/   /<?= $model->id ?>"> Mời
                                                    vào</a> -->
                                           


                                            <button id="btn_reject_get_ticket_<?= $model->id ?>"
                                                    class="btn btn-round btn-danger btn-sm"
                                                    onclick="process_reject_get_ticket(<?= $model->id ?>)" value="Hủy">
                                                Hủy
                                            </button>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>

</div>
<div id="create_note_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ghi chú</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div  class="col-sm-12 ">
                    <div class="form-group bmd-form-group">
                        <input id="form_car_number_hand" type="text" autocomplete="off" placeholder="Ghi chú"
                               style="text-transform:uppercase" class="form-control">
                        <span class="bmd-help">Nhập thông tin ghi chú</span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Trở về</button>
                <button id="process_ticket" type="button" onclick="process_update_note(<?= $model->id ?>)" class="btn btn-success">Lưu
                </button>
            </div>
        </div>
    </div>
</div>
<div id="print_order" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Nhập thông tin</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div  class="col-sm-12 ">
                            <div class="form-group bmd-form-group">
                                <input id="print_ticket_order" type="text" autocomplete="off" placeholder="Số lệnh"
                                       style="text-transform:uppercase" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Đóng</button>
                        <button type="button" onclick="process_print_ticket(<?= $model->id ?>)" class="btn btn-success">In Ticket
                        </button>
                    </div>
                </div>
            </div>
        </div>


<script>
    window.onload = function () {

    };
</script>
