<?php

use App\Helpers\Assets\AdminAsset;
use App\Helpers\Widgets\AdminAppBar;
use App\Helpers\Widgets\AdminNav;
use App\Helpers\SessionHelper;
use App\Models\SettingsModel;
use App\Helpers\Html;
use \App\Models\TgbxOrderDetailSortModel;
/**
 * @var $this \App\Libraries\BaseView
 * @var string $title
 * @var string $content
 * @var \CodeIgniter\HTTP\Request $request
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
//try{
AdminAsset::register($this);

//}catch (\Exception $ex){
//    var_dump($ex);
//}
//die;
$this->title = 'Thông Báo Lấy Hàng - Âm Thanh';
?>
<!doctype html>
<html>
<head>

    <title><?= $this->title ?></title>
    <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>
    <?php $this->head() ?>
</head>
<body>
<div class="wrapper ">
    <div class="row">
        <div class="col-md-12">

            <div class="card " style="margin-bottom: 0px">
                <div class="card-body text-center">
                    <div class="row text-center">
                        <div class="col-md-12">
                            <button id="btn_active_player" class="btn btn-primary" onclick="active_player()">Kich hoạt loa</button>
                        </div>

                    </div>
                    <div class="row text-center">
                        <div class="col-md-12">
                            <audio controls id="player">
                                <source id="playerSource" src=""></source>
                            </audio>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

<?php

$this->registerAssets();
?>
</body>

</html>
<script>
    var alert_message = undefined;
    var list_file = [];
    var audio = document.getElementById('player');
    audio.onended = function() {
        play_next();
    };
    window.onload = function() {


    };
    function active_player(){
        $('#btn_active_player').addClass('hidden');
        load_and_play();
    }

    function load_and_play(){
        list_file = get_list_file_play();
        // console.log("🚀 ~ file: take_item_alert_voice.php:92 ~ load_and_play ~ list_file:", list_file)
        var source = document.getElementById('playerSource');
        if(list_file){
            console.log("🚀 ~ file: take_item_alert_voice.php:95 ~ load_and_play ~ list_file:", list_file)
            source.src = list_file[0].file_alert;
            console.log("🚀 ~ file: take_item_alert_voice.php:96 ~ load_and_play ~ source.src:", source.src)
            source.data_id = list_file.id;
            // if(list_file.alert_back == 0 ){
            //     source.data_count = parseInt(list_file.alert_count);
            // }else {
            //     source.data_count = parseInt(list_file.alert_count_2);
            // }
            audio.load(); //call this to just preload the audio without playing
            audio.play(); //call this to play the song right away
        } else {
            source.src = null;
            setTimeout(function(){ load_and_play(); }, 3000);
        }
    }

    // function play_next(){
    //     //update count
    //     setTimeout(function(){ load_and_play(); }, 3000);
    //     // var source = document.getElementById('playerSource');
    //     // var remote_url = '/admin/Operator/get_config_voice/';
    //     // var number_alert = 2;
    //     // var alert_time_loop = 120*1000;
    //     // $.ajax({
    //     //     type: "GET",
    //     //     url: remote_url,
    //     //     async: false,
    //     //     success : function(data) {
    //     //         var config = JSON.parse(data);
    //     //         if(config){
    //     //             console.log(config);
    //     //             number_alert = parseInt(config.number_alert);
    //     //             alert_time_loop = parseInt(config.alert_time_loop)*1000;
    //     //         }
    //     //     }
    //     // });

    //     // if(source.data_count < number_alert){
    //     //     source.data_count = parseInt(source.data_count) + 1;
    //     //     update_file_play(source.data_id);
    //     //     console.log('after update',source.data_count);
    //     //     if(source.data_count < number_alert){
    //     //         setTimeout(function(){ audio.play(); },alert_time_loop);
    //     //     }else {
    //     //         setTimeout(function(){ load_and_play(); },5000);
    //     //     }
    //     // }else {
    //     //     setTimeout(function(){ load_and_play(); },5000);
    //     // }
    // }
    // function update_file_play(file_id){
    //     var remote_url ='/admin/Operator/update_play_voice_status/' +file_id;
    //     $.ajax({
    //         type: "POST",
    //         url: remote_url,
    //         async: false,
    //         success : function(data) {
    //         }
    //     });
    // }
    function get_list_file_play(){
        var remote_url ='/admin/Operator/get_list_voice_of_throad/' + '<?php echo $area_id; ?>'+'/'+'<?php echo $display_on; ?>';
        var list_voice = new Array();
        console.log("🚀 ~ file: take_item_alert_voice.php:156 ~ get_list_file_play ~ list_voice:", list_voice)
        $.ajax({
            type: "GET",
            url: remote_url,
            async: false,
            success : function(data) {
                list_voice = JSON.parse(data);
            }
        });
        return list_voice;
    }
    // sau 1p call 1 lan de check xem sau 10p thi xe da vao lay hang hay chua
    function check_voice_sort_after_time() {
        var remote_url ='/admin/Operator/check_voice_status_after_time/' + '<?php echo $area_id; ?>'+'/'+'<?php echo $display_on; ?>';
            $.ajax({
            type: "GET",
            url: remote_url,
            async: false,
            success : function(data) {
               
            }
           });
    }
    setInterval(() => {
        active_player();
    }, 60*1000);
    const time_check = 60*1000;
    setInterval(() => {
        check_voice_sort_after_time();
    }, time_check)

</script>