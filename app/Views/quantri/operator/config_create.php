<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Thêm cấu hình';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-info flex-align">
                <div>
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>

            </div>
            <div class="card-body">
                <form action="<?= route_to('nv_config_create') ?>" method="post"
                      enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">họng bơm</label>
                                <input type="text"  name="throad_name" autocomplete="off" class="form-control">
                            </div>
                            <div class="form-group" hidden>
                                <label class="bmd-label-floating">Kho</label>
                                <select class="form-control" name="area_id" >
                                    <option value="<?=$model_area->id?>"
                                        <?php if ($model_area->id == $model_name){
                                            echo 'selected';
                                        }?>
                                    ><?=$model_area->name?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="bmd-label-floating">Giàn bơm</label>
                                <select class="oneclick_create form-control" name="pump_system_id" id="pump_system_id"
                                        onchange="system_throad()">
                                    <?php if ($model_pump_system): ?>
                                        <?php foreach ($model_pump_system as $model_pms): ?>
                                            <option value="<?= $model_pms->id ?>"><?= $model_pms->pump_name ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="bmd-label-floating">Mặt hàng</label>
                                <select class="form-control" name="product_type_id">
                                    <?php if ($model_pump_prd): ?>
                                        <?php foreach ($model_pump_prd as $pump_prd): ?>
                                            <option value="<?= $pump_prd->id ?>"><?=$pump_prd->product_name?> - <?= $pump_prd->product_type_code ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Công suất(L/phút)</label>
                                <input type="number" value="<?=$model->wattage?>" name="wattage" autocomplete="off" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="bmd-label-floating">Màn hình</label>
                                <select class="form-control" name="display_on">
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="bmd-label-floating">Họng bơm song song</label>
                                <select class="form-control" name="thread_pararell_id" id="thread_pararell_id">

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-check">
                        <?= Html::hiddenInput('active', 0) ?>
                        <label class="form-check-label">
                            <?= Html::checkbox('active', $model->active == null ||($model->active == 1), [
                                'value' => 1, 'class' => 'form-check-input'
                            ]) ?> Hoạt động
                            <span class="form-check-sign"><span class="check"></span></span>
                        </label>
                    </div>

                    <?php if ($validator): ?>
                        <div class="alert alert-danger" style="margin-top: 32px;">
                            <ul style="margin: 0; padding-left: 16px;">
                                <?php foreach ($validator->getErrors() as $error): ?>
                                    <li><?= Html::decode($error) ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <div class="card-bottom-actions">
                        <div class="flex-row" id="add-holder"></div>
                        <a href="<?= route_to('nv_config') ?>" class="btn btn-round">Huỷ</a>
                        <button class="btn btn-info btn-round" type="submit">Lưu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<script>
    window.onload = function() {
        $('#pump_system_id').trigger('change');
    };
</script>
