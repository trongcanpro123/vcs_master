<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace App\Helpers;

use DateTime;

/**
 * Url provides a set of static methods for managing URLs.
 *
 * For more details and usage information on Url, see the [guide article on url helpers](guide:helper-url).
 *
 * @author Alexander Makarov <sam@rmcreative.ru>
 * @since 2.0
 */
class SettingHelper
{
    public function getSettingImage($image = ''): string
    {
        if (!$image || empty($image)) return '/images/empty.jpg';

        return base_url("uploads/content/{$image}");
    }

    public function getSettingImageLogo($image = ''): string
    {
        if (!$image || empty($image)) return '/images/empty.jpg';

        return base_url("images/{$image}");
    }

    public function getSettingImageFavicon($image = ''): string
    {
        if (!$image || empty($image)) return '/images/empty.jpg';

        return base_url("/{$image}");
    }

    public function save_base_64_to_file($image_base64)
    {
        list($type, $image_base64) = explode(';', $image_base64);
        list(, $image_base64) = explode(',', $image_base64);
        $image_base64 = base64_decode($image_base64);
	//echo '<pre>'; print_r($image_base64);die;

        $uploadPath = ROOTPATH . PUBLISH_FOLDER . '/uploads/content/';
	//echo '<pre>'; print_r($uploadPath);die;
        $file_name = $uploadPath . 'checkin_' . rand(0,1000).'_'. (new DateTime())->getTimestamp() . '.jpg';
	//echo '<pre>'; print_r($file_name);die;
        file_put_contents($file_name, $image_base64);
//	echo '<pre>'; print_r($file_name);die;
        return $file_name;
    }

    public function upload_base_64_to_file($image_base64)
    {
        list($type, $image_base64) = explode(';', $image_base64);
        list(, $image_base64) = explode(',', $image_base64);
        $image_base64 = base64_decode($image_base64);


        $uploadPath = ROOTPATH . PUBLISH_FOLDER . '/uploads/content/';
        $file_name = $uploadPath . 'checkin_' . rand(0,1000).'_'. (new DateTime())->getTimestamp() . '.jpg';
        file_put_contents($file_name, $image_base64);


        return str_replace(ROOTPATH . PUBLISH_FOLDER,'', $file_name);
    }

    public function api_detect_car_number($file_path)
    {

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => API_AI_BASE_URL . 'api/license_plate/recognize',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('image' => new \CURLFILE($file_path)),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        if ($response) {
            $response = json_decode($response);
            if($response->code == '200'){
                $response->data->text = str_replace('.','',$response->data->text);
                $response->data->text = str_replace('-','',$response->data->text);
                $response->data->text = str_replace('_','',$response->data->text);
                $response->data->text = str_replace('|','',$response->data->text);

            }
            return $response;
        }
        return null;

//        {
//            "code": 200,
//    "data": {
//            "detection_conf": "100",
//        "image_path": "static/images/license_plates/20210721_042719_4APT_lp.jpg",
//        "recognition_conf": "98",
//        "text": "562-A13939"
//    },
//    "message": "successfully recognized"
//}

    }

    public function api_detect_driver($file_path)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => API_AI_BASE_URL . 'api/face/recognize',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('image' => new \CURLFILE($file_path)),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        if ($response) {
            return json_decode($response);
        }
        return null;

        //        {
        //           "code": 200,
        //          "data": {
        //              "detection_conf": "100",
        //              "image_path": "static/images/faces/20210721_074543_VACC_face.jpg",
        //              "recognition_distance": "83",
        //              "user_id": "test_001"
        //              },
        //          "message": "successfully recognized"
        //}
    }

    public function api_register_driver($driver_id, $file_path)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => API_AI_BASE_URL . 'api/face/register',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('image' => new \CURLFILE($file_path), 'user_id' => $driver_id),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        if ($response) {
            return json_decode($response);
        }
        return null;
    }

    public function api_delete_driver($driver_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => API_AI_BASE_URL . '/api/face/delete',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array( 'user_id' => $driver_id),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        if ($response) {
            return json_decode($response);
        }
        return null;
    }

    public function api_smo_info($qr_code, $area_code){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  API_SMO_BX_BASE_URL . 'smo',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('qr_code' => $qr_code, 'area_code' =>$area_code),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        if ($response) {
            return json_decode($response);
        }
        return null;
    }

    public function api_bx_get_order($car_number,$area_code){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  API_SMO_BX_BASE_URL . 'tgbx/get_order',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('car_number' => $car_number,'area_code' => $area_code),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        if ($response) {
            return json_decode($response,true);
        }
        return null;
    }

    public function api_tdh_check_status($car_number,$area_code, $ma_lenh,$ngay_xuat, $ma_hang_hoa){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  API_SMO_BX_BASE_URL . 'tdh/check_status',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('car_number' => $car_number,'area_code' => $area_code,
                'ma_lenh' => $ma_lenh,
                'ngay_xuat'=> $ngay_xuat,
                'ma_hang_hoa' => $ma_hang_hoa),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        if ($response) {
            return json_decode($response,true);
        }
        return null;
    }
    function api_send_sms($car_number, $area_id)

    {

        $data = array("carNumber" => $car_number, "areaId" => $area_id);

        $data_string = json_encode($data);

        // echo print_r($data_string);die;



        $curl = curl_init('http://10.6.8.15:5050/vcs-sms');



        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(

            'Content-Type: application/json',

            'Content-Length: ' . strlen($data_string))

        );

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result) {

            return json_decode($result, true);

        }

        return null;

    }

    function api_vcs_tiket($car_number, $checkin_time, $area_id)

    {

        $data = array("carNumber" => $car_number, "checkinTime" => $checkin_time, "areaId" => $area_id);

        $data_string = json_encode($data);

        // echo print_r($data_string);die;



        $curl = curl_init('http://10.6.8.15:5000/vcs-tiket');



        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(

            'Content-Type: application/json',

            'Content-Length: ' . strlen($data_string))

        );

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result) {

            return json_decode($result, true);

        }

        return null;

    }
    function api_vcs_arrange($car_number, $id, $area_id)

    {

        $data = array("carNumber" => $car_number, "checkinTime" => $id, "areaId" => $area_id);

        $data_string = json_encode($data);

        // echo print_r($data_string);die;



        $curl = curl_init('http://10.6.8.15:5000/vcs-arrange');



        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(

            'Content-Type: application/json',

            'Content-Length: ' . strlen($data_string))

        );

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result) {

            return json_decode($result, true);

        }

        return null;

    }

    function api_vcs_checkout($car_number, $in_out_id, $area_id)

    {

        $data = array("carNumber" => $car_number, "checkinTime" => $in_out_id, "areaId" => $area_id);

        $data_string = json_encode($data);

        // echo print_r($data_string);die;



        $curl = curl_init('http://10.6.8.15:5000/vcs-checkout');



        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(

            'Content-Type: application/json',

            'Content-Length: ' . strlen($data_string))

        );

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result) {

            return json_decode($result, true);

        }

        return null;

    }
    
    function api_vcs_checkStore($car_number, $area_id)

    {

        $data = array("carNumber" => $car_number, "areaId" => $area_id);

        $data_string = json_encode($data);

        // echo print_r($data_string);die;



        $curl = curl_init('http://10.6.8.15:5000/vcs-checkStore');



        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(

            'Content-Type: application/json',

            'Content-Length: ' . strlen($data_string))

        );

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result) {

            return json_decode($result, true);

        }

        return null;

    }

    function api_vcs_car_arrangement($area_id)

    {

         $data = array("areaId" => $area_id);

         $data_string = json_encode($data);

       //  echo print_r($data_string);die;



        $curl = curl_init('http://10.6.8.15:5000/vcs-arrangement');



        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(

             'Content-Type: application/json',

            'Content-Length: ' . strlen($data_string))

         );

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result) {

            return json_decode($result, true);

        }

        return null;

    }
    function api_vcs_process($area_id)

    {

        $data = array("areaId" => $area_id);

        $data_string = json_encode($data);

        // echo print_r($data_string);die;



        $curl = curl_init('http://10.6.8.15:5000/vcs-process');



        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(

            'Content-Type: application/json',

            'Content-Length: ' . strlen($data_string))

        );

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result) {

            return json_decode($result, true);

        }

        return null;

    }
    function api_vcs_print($area_id, $car_number)

    {

        $data = array("areaId" => $area_id, 'carNumber' => $car_number);

        $data_string = json_encode($data);

        // echo print_r($data_string);die;



        $curl = curl_init('http://10.6.8.15:5000/vcs-print');



        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(

            'Content-Type: application/json',

            'Content-Length: ' . strlen($data_string))

        );

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result) {

            return json_decode($result, true);

        }

        return null;

    }
    function api_vcs_print_order($solenh,$area_id)

    {

        $data = array("solenh"=>$solenh, "areaId" => $area_id);

        $data_string = json_encode($data);

        // echo print_r($data_string);die;



        $curl = curl_init('http://10.6.8.15:5000/vcs-print-order');



        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(

            'Content-Type: application/json',

            'Content-Length: ' . strlen($data_string))

        );

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result) {

            return json_decode($result, true);

        }

        return null;

    }
    function api_vcs_number($area_id)

    {

        $data = array("areaId" => $area_id);

        $data_string = json_encode($data);

        // echo print_r($data_string);die;



        $curl = curl_init('http://10.6.8.15:5000/vcs-number');



        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(

            'Content-Type: application/json',

            'Content-Length: ' . strlen($data_string))

        );

        $result = curl_exec($curl);

        curl_close($curl);

        if ($result) {

            return json_decode($result, true);

        }

        return null;

    }

}
