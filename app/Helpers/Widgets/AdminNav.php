<?php

namespace App\Helpers\Widgets;

use App\Helpers\Assets\MaterialDashboard;
use App\Libraries\BaseView;
use App\Models\AdministratorModel;

class AdminNav extends BaseWidget
{
    private static $items = [
//        'admin_home' => [
//            'label' => 'Trang chủ', 'icon' => 'dashboard', 'ns' => null
//        ],
//        'admin_slider' => [
//            'label' => 'Slider', 'icon' => 'queue_play_next', 'ns' => 'slider'
//        ],
//        'admin_category' => [
//            'label' => 'Danh mục bài viết', 'icon' => 'chrome_reader_mode', 'ns' => 'category'
//        ],
//        'admin_project_category' => [
//            'label' => 'Danh mục dự án', 'icon' => 'share', 'ns' => 'project-category'
//        ],
//        'admin_project' => [
//            'label' => 'Dự án', 'icon' => 'business', 'ns' => 'project'
//        ],
//        'admin_product_category' => [
//            'label' => 'Danh mục sản phẩm', 'icon' => 'share', 'ns' => 'product-category'
//        ],
//        'admin_product' => [
//            'label' => 'Sản phẩm', 'icon' => 'store_mall_direction', 'ns' => 'product'
//        ],
//        'admin_content' => [
//            'label' => 'Nội dung', 'icon' => 'chrome_reader_mode', 'ns' => 'content'
//        ],
//        'admin_news' => [
//            'label' => 'Tin tức', 'icon' => 'chrome_reader_mode', 'ns' => 'news'
//        ],
//        'admin_testimonial' => [
//            'label' => 'Khách hàng nhận xét', 'icon' => 'people', 'ns' => 'testimonial'
//        ],
//        'admin_partner' => [
//            'label' => 'Đối tác', 'icon' => 'extension', 'ns' => 'partner'
//        ],
//        'admin_user_request' => [
//            'label' => 'Yêu cầu gọi lại', 'icon' => 'ring_volume', 'ns' => 'user-request'
//        ],
//        'admin_cart' => [
//            'label' => 'Đơn hàng', 'icon' => 'shopping_cart', 'ns' => 'cart'
//        ],
//        'admin_setting' => [
//            'label' => 'Cấu hình chung', 'icon' => 'settings_applications', 'ns' => 'settings'
//        ],
        'administrator' => [
            'label' => 'Quản trị viên', 'icon' => 'how_to_reg', 'ns' => 'administrator'
        ],

    ];

    private static $lanh_dao_items = [
        //day la 2 trang vi du
        'ld_home' => [
            'label' => 'Trang chủ', 'icon' => 'dashboard', 'ns' => 'lanh-dao-trang-chu'
        ],
        'ld_in_out_detail' => [
            'label' => 'Chi tiết vào ra', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-chi-tiet-vao-ra'
        ],
        'ld_report_car_detail' => [
            'label' => 'Báo cáo xe chi tiết', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-xe-chi-tiet'
        ],
        'ld_report_product_detail' => [
            'label' => 'Báo cáo sản phẩm chi tiết', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-sp-chi-tiet'
        ],
        'ld_report_throad_detail' => [
            'label' => 'Báo cáo họng chi tiết', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-hong-chi-tiet'
        ],
        'ld_report_car_total' => [
            'label' => 'Báo cáo xe tổng hợp', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-xe-tong-hop'
        ],
        'ld_report_product_total' => [
            'label' => 'Báo cáo sản phẩm tổng hợp', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-sp-tong-hop'
        ],
        'ld_report_throad_total' => [
            'label' => 'Báo cáo họng tổng hợp', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-hong-tong-hop'
        ],
        'administrator' => [
            'label' => 'Quản lý người dùng', 'icon' => 'chrome_reader_mode', 'ns' => 'administrator'
        ],
        'ld_voice_option' => [
            'label' => 'Cấu hình âm thanh', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-voice-option'
        ],
        //thêm các menu của lãnh đạo dưới này và bổ sung route, chú ý tên route as phải giống với ns trong items
    ];

    private static $lanh_dao_kho_items = [
        //day la 2 trang vi du
        'ld_home' => [
            'label' => 'Trang chủ', 'icon' => 'dashboard', 'ns' => 'lanh-dao-trang-chu'
        ],
        'ld_in_out_detail' => [
            'label' => 'Chi tiết vào ra', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-chi-tiet-vao-ra'
        ],
        'ld_report_car_detail' => [
            'label' => 'Báo cáo xe chi tiết', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-xe-chi-tiet'
        ],
        'ld_report_product_detail' => [
            'label' => 'Báo cáo sản phẩm chi tiết', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-sp-chi-tiet'
        ],
        'ld_report_throad_detail' => [
            'label' => 'Báo cáo họng chi tiết', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-hong-chi-tiet'
        ],
        'ld_report_car_total' => [
            'label' => 'Báo cáo xe tổng hợp', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-xe-tong-hop'
        ],
        'ld_report_product_total' => [
            'label' => 'Báo cáo sản phẩm tổng hợp', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-sp-tong-hop'
        ],
        'ld_report_throad_total' => [
            'label' => 'Báo cáo họng tổng hợp', 'icon' => 'chrome_reader_mode', 'ns' => 'lanh-dao-bc-hong-tong-hop'
        ],
        'nv_order_take_ticket' => [
            'label' => 'Thứ tự lấy ticket', 'icon' => 'dashboard', 'ns' => 'nhan-vien-thu-tu-lay-ticket'
        ],
        //thêm các menu của lãnh đạo dưới này và bổ sung route, chú ý tên route as phải giống với ns trong items
    ];

    private static $nhan_vien_items = [
        'nv_order_take_ticket' => [
            'label' => 'Thứ tự lấy ticket', 'icon' => 'dashboard', 'ns' => 'nhan-vien-thu-tu-lay-ticket'
        ],
        'nv_print_ticket' => [
            'label' => 'In ticket', 'icon' => 'chrome_reader_mode', 'ns' => 'nhan-vien-in-ticket'
        ],
        'nv_order_take_item' => [
            'label' => 'Thứ tự lấy hàng', 'icon' => 'chrome_reader_mode', 'ns' => 'nhan-vien-thu-tu-lay-hang'
        ],
        'nv_config' => [
            'label' => 'Cấu hình họng bơm', 'icon' => 'chrome_reader_mode', 'ns' => 'nhan-vien-cau-hinh'
        ],
        'nv_system' => [
            'label' => 'Quản lý giàn bơm', 'icon' => 'chrome_reader_mode', 'ns' => 'nhan-vien-gian-bom'
        ],
        'nv_pump_product_type' => [
            'label' => 'Quản lý mặt hàng', 'icon' => 'chrome_reader_mode', 'ns' => 'nhan-vien-mat-hang'
        ],
        'nv_help' => [
            'label' => 'Trợ giúp', 'icon' => 'chrome_reader_mode', 'ns' => 'nhan-vien-tro-giup'
        ],
    ];

    private static $bao_ve_items = [
        'bv_in_out_manager' => [
            'label' => 'Kiểm soát vào ra', 'icon' => 'dashboard', 'ns' => 'bao-ve-kiem-soat-vao-ra'
        ],
        'bv_in_out_history' => [
            'label' => 'Lịch sử vào ra', 'icon' => 'chrome_reader_mode', 'ns' => 'lich-su-vao-ra'
        ],
        'bv_history_sms' => [
            'label' => 'Lịch sử sms', 'icon' => 'chrome_reader_mode', 'ns' => 'lich-su-sms'
        ],
        'driver' => [
            'label' => 'Danh sách tài xế', 'icon' => 'chrome_reader_mode', 'ns' => 'danh-sach-tai-xe'
        ],
        'bv_list_cars' => [
            'label' => 'Danh sách xe', 'icon' => 'chrome_reader_mode', 'ns' => 'danh-sach-xe'
        ],
        'bv_help' => [
            'label' => 'Trợ giúp', 'icon' => 'chrome_reader_mode', 'ns' => 'bao-ve-tro-giup'
        ],
    ];

    public static function register(BaseView $view, array $data = []): string
    {

        $asset = MaterialDashboard::getAsset();
        $user = AdministratorModel::findIdentity();
//        var_dump($identity);die;
//
//        $user = (new AdministratorModel())->where('id',$identity)->first();
        if ($user->type_user == 'admin'){
            return parent::render($view, 'admin_nav', array_merge($data, [
                'items' => static::$items,
                'asset' => $asset
            ]));
        }
        if ($user->type_user == 'lanh_dao'){
            return parent::render($view, 'admin_nav', array_merge($data, [
                'items' => static::$lanh_dao_items,
                'asset' => $asset
            ]));
        }
        if ($user->type_user == 'lanh_dao_kho'){
            return parent::render($view, 'admin_nav', array_merge($data, [
                'items' => static::$lanh_dao_kho_items,
                'asset' => $asset
            ]));
        }
        if ($user->type_user == 'nhan_vien'){
            return parent::render($view, 'admin_nav', array_merge($data, [
                'items' => static::$nhan_vien_items,
                'asset' => $asset
            ]));
        }
        if ($user->type_user == 'bao_ve'){
            return parent::render($view, 'admin_nav', array_merge($data, [
                'items' => static::$bao_ve_items,
                'asset' => $asset
            ]));
        }
    }
}