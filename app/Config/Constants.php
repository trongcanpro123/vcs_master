<?php

//--------------------------------------------------------------------
// App Namespace
//--------------------------------------------------------------------
// This defines the default Namespace that is used throughout
// CodeIgniter to refer to the Application directory. Change
// this constant to change the namespace that all application
// classes should use.
//
// NOTE: changing this will require manually modifying the
// existing namespaces of App\* namespaced-classes.
//
defined('APP_NAMESPACE') || define('APP_NAMESPACE', 'App');

/*
|--------------------------------------------------------------------------
| Composer Path
|--------------------------------------------------------------------------
|
| The path that Composer's autoload file is expected to live. By default,
| the vendor folder is in the Root directory, but you can customize that here.
*/
defined('COMPOSER_PATH') || define('COMPOSER_PATH', ROOTPATH . 'vendor/autoload.php');

/*
|--------------------------------------------------------------------------
| Timing Constants
|--------------------------------------------------------------------------
|
| Provide simple ways to work with the myriad of PHP functions that
| require information to be in seconds.
*/
defined('SECOND') || define('SECOND', 1);
defined('MINUTE') || define('MINUTE', 60);
defined('HOUR')   || define('HOUR', 3600);
defined('DAY')    || define('DAY', 86400);
defined('WEEK')   || define('WEEK', 604800);
defined('MONTH')  || define('MONTH', 2592000);
defined('YEAR')   || define('YEAR', 31536000);
defined('DECADE') || define('DECADE', 315360000);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        || define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          || define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         || define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   || define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  || define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') || define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     || define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       || define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      || define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      || define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


defined('ADMIN_NAME')          || define('ADMIN_NAME', 'Panda CMS'); // CMS name
defined('ADMIN_PATH')          || define('ADMIN_PATH', 'quantri'); // admin base path
defined('PWD_SALT')            || define('PWD_SALT', 'Ac1disvFdxMDdmjOsdpecFw'); // admin base path
defined('PUBLISH_FOLDER')      || define('PUBLISH_FOLDER', 'public_html');
defined('API_AI_BASE_URL')      || define('API_AI_BASE_URL', 'http://10.6.20.10:8864/');
defined('MODE_TEST')      || define('MODE_TEST', false);
defined('API_SMO_BX_BASE_URL')      || define('API_SMO_BX_BASE_URL', 'http://localhost:8001/');

defined('PREFIX_CHECKOUT_CAMERA_IMG')      || define('PREFIX_CHECKOUT_CAMERA_IMG', 'checkout_image_capture_');
defined('PREFIX_CHECKOUT_AI_IMG')      || define('PREFIX_CHECKOUT_AI_IMG', 'checkout_ai_image_');
defined('PREFIX_CHECKOUT_AI_CAR_NUMBER')      || define('PREFIX_CHECKOUT_AI_CAR_NUMBER', 'checkout_ai_car_number_');
defined('PREFIX_CHECKOUT_ALERT')      || define('PREFIX_CHECKOUT_ALERT', 'checkout_alert_');

defined('RGB')      || define('RGB', [
       'rgb(255, 195, 77)',
       'rgb(0, 204, 0)',
       'rgb(153, 153, 100)',
       'rgb(153, 153, 90)',
       'rgb(153, 153, 102)',
       'rgb(44, 24, 69)',
       'rgb(235, 64, 52)',
       'rgb(32, 24, 69)',
       'rgb(235, 217, 52)',
       'rgb(32, 24, 69)',
       'rgb(166, 162, 126)',
        'rgb(44, 24, 69)',
        'rgb(40, 24, 69)',
        'rgb(69, 24, 53)',
        'rgb(31, 69, 24)',
        'rgb(32, 50, 69)',
]);

defined('CHECKOUT_ALERT_SECOND')      || define('CHECKOUT_ALERT_SECOND', '60');
defined('TAKE_TICKET_ALERT_MINUTES')      || define('TAKE_TICKET_ALERT_MINUTES', '5');
defined('TAKE_ITEM_ALERT_MINUTES')      || define('TAKE_ITEM_ALERT_MINUTES', '5');
