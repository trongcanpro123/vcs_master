<?php namespace Config;

use App\Helpers\StringHelper;
use App\Models\AdministratorModel;
use App\Models\RouterUrlModel;
use CodeIgniter\Router\RouteCollection;

/**
 * --------------------------------------------------------------------
 * URI Routing
 * --------------------------------------------------------------------
 * This file lets you re-map URI requests to specific controller functions.
 *
 * Typically there is a one-to-one relationship between a URL string
 * and its corresponding controller class/method. The segments in a
 * URL normally follow this pattern:
 *
 *    example.com/class/method/id
 *
 * In some instances, however, you may want to remap this relationship
 * so that a different class/function is called than the one
 * corresponding to the URL.
 */

// Create a new instance of our RouteCollection class.
$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 * The RouteCollection object allows you to modify the way that the
 * Router works, by acting as a holder for it's configuration settings.
 * The following methods can be called on the object to modify
 * the default operations.
 *
 *    $routes->defaultNamespace()
 *
 * Modifies the namespace that is added to a controller if it doesn't
 * already have one. By default this is the global namespace (\).
 *
 *    $routes->defaultController()
 *
 * Changes the name of the class used as a controller when the route
 * points to a folder instead of a class.
 *
 *    $routes->defaultMethod()
 *
 * Assigns the method inside the controller that is ran when the
 * Router is unable to determine the appropriate method to run.
 *
 *    $routes->setAutoRoute()
 *
 * Determines whether the Router will attempt to match URIs to
 * Controllers when no specific route has been defined. If false,
 * only routes that have been defined here will be available.
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// Admin router
$routes->group(ADMIN_PATH, function (RouteCollection $routes) {
    $routes->add('/', 'App\Controllers\Admin\Home::index', ['as' => 'admin_home']);

    // Auth
    $routes->add('auth/initialize', 'App\Controllers\Admin\Auth::initialize', [
        'as' => 'admin_initialize'
    ]);
    $routes->add('auth/login', 'App\Controllers\Admin\Auth::login', [
        'as' => 'admin_login'
    ]);
    $routes->add('auth/logout', 'App\Controllers\Admin\Auth::logout', [
        'as' => 'admin_logout'
    ]);

    // Slider
    $routes->add('slider', 'App\Controllers\Admin\Slider::index', [
        'as' => 'admin_slider'
    ]);
    $routes->add('slider/create', 'App\Controllers\Admin\Slider::create', [
        'as' => 'admin_slider_create'
    ]);
    $routes->add('slider/update/(:num)', 'App\Controllers\Admin\Slider::update/$1', [
        'as' => 'admin_slider_update'
    ]);
    $routes->add('slider/delete/(:num)', 'App\Controllers\Admin\Slider::delete/$1', [
        'as' => 'admin_slider_delete'
    ]);

    // Content Category
    $routes->add('category', 'App\Controllers\Admin\Category::index', [
        'as' => 'admin_category'
    ]);
    $routes->add('category/create', 'App\Controllers\Admin\Category::create', [
        'as' => 'admin_category_create'
    ]);
    $routes->add('category/update/(:num)', 'App\Controllers\Admin\Category::update/$1', [
        'as' => 'admin_category_update'
    ]);
    $routes->add('category/delete/(:num)', 'App\Controllers\Admin\Category::delete/$1', [
        'as' => 'admin_category_delete'
    ]);

    // Project Category
    $routes->add('project-category', 'App\Controllers\Admin\ProjectCategory::index', [
        'as' => 'admin_project_category'
    ]);
    $routes->add('project-category/create', 'App\Controllers\Admin\ProjectCategory::create', [
        'as' => 'admin_project_category_create'
    ]);
    $routes->add('project-category/update/(:num)', 'App\Controllers\Admin\ProjectCategory::update/$1', [
        'as' => 'admin_project_category_update'
    ]);
    $routes->add('project-category/delete/(:num)', 'App\Controllers\Admin\ProjectCategory::delete/$1', [
        'as' => 'admin_project_category_delete'
    ]);

    // Project
    $routes->add('project', 'App\Controllers\Admin\Project::index', [
        'as' => 'admin_project'
    ]);
    $routes->add('project/create', 'App\Controllers\Admin\Project::create', [
        'as' => 'admin_project_create'
    ]);
    $routes->add('project/update/(:num)', 'App\Controllers\Admin\Project::update/$1', [
        'as' => 'admin_project_update'
    ]);
    $routes->add('project/delete/(:num)', 'App\Controllers\Admin\Project::delete/$1', [
        'as' => 'admin_project_delete'
    ]);

    // Product Category
    $routes->add('product-category', 'App\Controllers\Admin\ProductCategory::index', [
        'as' => 'admin_product_category'
    ]);
    $routes->add('product-category/create', 'App\Controllers\Admin\ProductCategory::create', [
        'as' => 'admin_product_category_create'
    ]);
    $routes->add('product-category/update/(:num)', 'App\Controllers\Admin\ProductCategory::update/$1', [
        'as' => 'admin_product_category_update'
    ]);
    $routes->add('product-category/delete/(:num)', 'App\Controllers\Admin\ProductCategory::delete/$1', [
        'as' => 'admin_product_category_delete'
    ]);

    // Product
    $routes->add('product', 'App\Controllers\Admin\Product::index', [
        'as' => 'admin_product'
    ]);
    $routes->add('product/create', 'App\Controllers\Admin\Product::create', [
        'as' => 'admin_product_create'
    ]);
    $routes->add('product/update/(:num)', 'App\Controllers\Admin\Product::update/$1', [
        'as' => 'admin_product_update'
    ]);
    $routes->add('product/delete/(:num)', 'App\Controllers\Admin\Product::delete/$1', [
        'as' => 'admin_product_delete'
    ]);

    // Content
    $routes->add('content', 'App\Controllers\Admin\Content::index', [
        'as' => 'admin_content'
    ]);
    $routes->add('content/create', 'App\Controllers\Admin\Content::create', [
        'as' => 'admin_content_create'
    ]);
    $routes->add('content/update/(:num)', 'App\Controllers\Admin\Content::update/$1', [
        'as' => 'admin_content_update'
    ]);
    $routes->add('content/delete/(:num)', 'App\Controllers\Admin\Content::delete/$1', [
        'as' => 'admin_content_delete'
    ]);
    $routes->add('content/meta/(:any)/(:num)', 'App\Controllers\Admin\Content::meta/$1/$2', [
        'as' => 'admin_content_meta'
    ]);

    // News
    $routes->add('news', 'App\Controllers\Admin\News::index', [
        'as' => 'admin_news'
    ]);
    $routes->add('news/create', 'App\Controllers\Admin\News::create', [
        'as' => 'admin_news_create'
    ]);
    $routes->add('news/update/(:num)', 'App\Controllers\Admin\News::update/$1', [
        'as' => 'admin_news_update'
    ]);
    $routes->add('news/delete/(:num)', 'App\Controllers\Admin\News::delete/$1', [
        'as' => 'admin_news_delete'
    ]);

    // Testimonial
    $routes->add('testimonial', 'App\Controllers\Admin\Testimonial::index', [
        'as' => 'admin_testimonial'
    ]);
    $routes->add('testimonial/create', 'App\Controllers\Admin\Testimonial::create', [
        'as' => 'admin_testimonial_create'
    ]);
    $routes->add('testimonial/update/(:num)', 'App\Controllers\Admin\Testimonial::update/$1', [
        'as' => 'admin_testimonial_update'
    ]);
    $routes->add('testimonial/delete/(:num)', 'App\Controllers\Admin\Testimonial::delete/$1', [
        'as' => 'admin_testimonial_delete'
    ]);

    // Partner
    $routes->add('partner', 'App\Controllers\Admin\Partner::index', [
        'as' => 'admin_partner'
    ]);
    $routes->add('partner/create', 'App\Controllers\Admin\Partner::create', [
        'as' => 'admin_partner_create'
    ]);
    $routes->add('partner/update/(:num)', 'App\Controllers\Admin\Partner::update/$1', [
        'as' => 'admin_partner_update'
    ]);
    $routes->add('partner/delete/(:num)', 'App\Controllers\Admin\Partner::delete/$1', [
        'as' => 'admin_partner_delete'
    ]);

    // User Request
    $routes->add('user-request', 'App\Controllers\Admin\UserRequest::index', [
        'as' => 'admin_user_request'
    ]);
    $routes->add('user-request/view/(:num)', 'App\Controllers\Admin\UserRequest::view/$1', [
        'as' => 'admin_user_request_view'
    ]);
    $routes->add('user-request/update/(:num)', 'App\Controllers\Admin\UserRequest::update/$1', [
        'as' => 'admin_user_request_update'
    ]);
    $routes->add('user-request/delete/(:num)', 'App\Controllers\Admin\UserRequest::delete/$1', [
        'as' => 'admin_user_request_delete'
    ]);

    // Shopping Cart
    $routes->add('cart', 'App\Controllers\Admin\ShoppingCart::index', [
        'as' => 'admin_cart'
    ]);
    $routes->add('cart/view/(:num)', 'App\Controllers\Admin\ShoppingCart::view/$1', [
        'as' => 'admin_cart_view'
    ]);
    $routes->add('cart/update/(:num)', 'App\Controllers\Admin\ShoppingCart::update/$1', [
        'as' => 'admin_cart_update'
    ]);
    $routes->add('cart/delete/(:num)', 'App\Controllers\Admin\ShoppingCart::delete/$1', [
        'as' => 'admin_cart_delete'
    ]);

    //setting
    $routes->add('settings', 'App\Controllers\Admin\Settings::index', [
        'as' => 'admin_setting'
    ]);
    $routes->add('settings/update', 'App\Controllers\Admin\Settings::update', [
        'as' => 'admin_settings_update'
    ]);


    // Lãnh đạo
    $routes->add('administrator', 'App\Controllers\Admin\Administrator::index', [
        'as' => 'administrator'
    ]);
    $routes->add('administrator/create', 'App\Controllers\Admin\Administrator::create', [
        'as' => 'administrator_create'
    ]);
    $routes->add('administrator/update/(:num)', 'App\Controllers\Admin\Administrator::update/$1', [
        'as' => 'administrator_update'
    ]);
    $routes->add('administrator/delete/(:num)', 'App\Controllers\Admin\Administrator::delete/$1', [
        'as' => 'administrator_delete'
    ]);
    $routes->add('administrator/password/(:num)', 'App\Controllers\Admin\Administrator::password/$1', [
        'as' => 'administrator_password'
    ]);
    $routes->add('administrator/change_password', 'App\Controllers\Admin\Administrator::change_password', [
        'as' => 'administrator_change_password'
    ]);

    $routes->add('lanh-dao-trang-chu', 'App\Controllers\Admin\Administrator::lanh_dao_index', [
        'as' => 'ld_home'
    ]);
    $routes->add('lanh-dao-chi-tiet-vao-ra', 'App\Controllers\Admin\Administrator::in_out_detail', [
        'as' => 'ld_in_out_detail'
    ]);
    $routes->add('lanh-dao-bc-xe-chi-tiet', 'App\Controllers\Admin\Administrator::report_car_detail', [
        'as' => 'ld_report_car_detail'
    ]);

    $routes->add('lanh-dao-bc-sp-chi-tiet', 'App\Controllers\Admin\Administrator::report_product_detail', [
        'as' => 'ld_report_product_detail'
    ]);
    $routes->add('lanh-dao-bc-hong-chi-tiet', 'App\Controllers\Admin\Administrator::report_throad_detail', [
        'as' => 'ld_report_throad_detail'
    ]);
    $routes->add('lanh-dao-bc-xe-tong-hop', 'App\Controllers\Admin\Administrator::report_car_total', [
        'as' => 'ld_report_car_total'
    ]);
    $routes->add('lanh-dao-bc-sp-tong-hop', 'App\Controllers\Admin\Administrator::report_product_total', [
        'as' => 'ld_report_product_total'
    ]);

    $routes->add('lanh-dao-bc-hong-tong-hop', 'App\Controllers\Admin\Administrator::report_throad_total', [
        'as' => 'ld_report_throad_total'
    ]);

    $routes->add('lanh-dao-voice-option', 'App\Controllers\Admin\Administrator::voice_option', [
        'as' => 'ld_voice_option'
    ]);

    $routes->add('danh-sach-carmera', 'App\Controllers\Admin\Administrator::load_camera_by_area', [
        'as' => 'load_camera_by_area'
    ]);

    //nhan vien
    $routes->add('nhan-vien-thu-tu-lay-ticket', 'App\Controllers\Admin\Operator::order_take_ticket', [
        'as' => 'nv_order_take_ticket'
    ]);
    $routes->add('nhan-vien-in-ticket', 'App\Controllers\Admin\Operator::print_ticket', [
        'as' => 'nv_print_ticket'
    ]);
    $routes->add('nhan-vien-thu-tu-lay-hang', 'App\Controllers\Admin\Operator::order_take_item', [
        'as' => 'nv_order_take_item'
    ]);
    //config
    $routes->add('nhan-vien-cau-hinh', 'App\Controllers\Admin\Operator::config', [
        'as' => 'nv_config'
    ]);
    $routes->add('operator/create', 'App\Controllers\Admin\Operator::config_create', [
        'as' => 'nv_config_create'
    ]);
    $routes->add('operator/update/(:num)', 'App\Controllers\Admin\Operator::config_update/$1', [
        'as' => 'nv_config_update'
    ]);
    $routes->add('operator/delete/(:num)', 'App\Controllers\Admin\Operator::config_delete/$1', [
        'as' => 'nv_config_delete'
    ]);



    //giàn bơm
    $routes->add('nhan-vien-gian-bom', 'App\Controllers\Admin\PumpSystem::index', [
        'as' => 'nv_system'
    ]);
    $routes->add('pump-system/create', 'App\Controllers\Admin\PumpSystem::create', [
        'as' => 'nv_pump_system_create'
    ]);
    $routes->add('pump-system/update/(:num)', 'App\Controllers\Admin\PumpSystem::update/$1', [
        'as' => 'nv_pump_system_update'
    ]);
    $routes->add('pump-system/delete/(:num)', 'App\Controllers\Admin\PumpSystem::delete/$1', [
        'as' => 'nv_pump_system_delete'
    ]);
    $routes->add('nhan-vien-tro-giup', 'App\Controllers\Admin\Operator::help', [
        'as' => 'nv_help'
    ]);
    //mặt hàng
    $routes->add('nhan-vien-mat-hang', 'App\Controllers\Admin\PumpProductType::index', [
        'as' => 'nv_pump_product_type'
    ]);
    $routes->add('pump-product-type/create', 'App\Controllers\Admin\PumpProductType::create', [
        'as' => 'nv_pump_product_type_create'
    ]);
    $routes->add('pump-product-type/update/(:num)', 'App\Controllers\Admin\PumpProductType::update/$1', [
        'as' => 'nv_pump_product_type_update'
    ]);
    $routes->add('pump-product-type/delete/(:num)', 'App\Controllers\Admin\PumpProductType::delete/$1', [
        'as' => 'nv_pump_product_type_delete'
    ]);

    //bao ve
    
    $routes->add('bao-ve-kiem-soat-vao-ra', 'App\Controllers\Admin\GateKeeper::in_out_manager', [
        'as' => 'bv_in_out_manager'
    ]);

    $routes->add('lich-su-vao-ra', 'App\Controllers\Admin\GateKeeper::in_out_history', [
        'as' => 'bv_in_out_history'
    ]);
    $routes->add('lich-su-sms', 'App\Controllers\Admin\GateKeeper::history_sms', [
        'as' => 'bv_history_sms'
    ]);

    $routes->add('danh-sach-tai-xe', 'App\Controllers\Admin\GateKeeper::index', [
        'as' => 'driver'
    ]);

    $routes->add('car/create', 'App\Controllers\Admin\GateKeeper::bv_create_car', [
        'as' => 'admin_car_create'
    ]);
    $routes->add('car/create/(:num)', 'App\Controllers\Admin\GateKeeper::bv_create_car/$1', [
        'as' => 'admin_car_create_1'
    ]);
    $routes->add('car/create/(:num)/(:any)', 'App\Controllers\Admin\GateKeeper::bv_create_car/$1/$2', [
        'as' => 'admin_car_create_2'
    ]);

    $routes->add('car/update/(:num)', 'App\Controllers\Admin\GateKeeper::bv_update_car/$1', [
        'as' => 'admin_car_update'
    ]);

    $routes->add('car/delete/(:num)', 'App\Controllers\Admin\GateKeeper::bv_delete_car/$1', [
        'as' => 'admin_car_delete'
    ]);

    $routes->add('driver/create', 'App\Controllers\Admin\GateKeeper::create', [
         'as' => 'admin_driver_create'
    ]);
    $routes->add('driver/create/(:num)', 'App\Controllers\Admin\GateKeeper::create/$1', [
        'as' => 'admin_driver_create_1'
    ]);

    $routes->add('driver/delete/(:num)', 'App\Controllers\Admin\GateKeeper::delete/$1', [
        'as' => 'admin_driver_delete'
    ]);


    $routes->add('driver/update/(:num)', 'App\Controllers\Admin\GateKeeper::update/$1', [
        'as' => 'admin_driver_update'
    ]);

    $routes->add('driver/delete/(:num)', 'App\Controllers\Admin\GateKeeper::delete/$1', [
        'as' => 'admin_driver_delete'
    ]);

    $routes->add('danh-sach-xe', 'App\Controllers\Admin\GateKeeper::show_all_list_cars', [
        'as' => 'bv_list_cars'
    ]);

    $routes->add('bao-ve-tro-giup', 'App\Controllers\Admin\GateKeeper::help', [
        'as' => 'bv_help'
    ]);

    $routes->add('xu-ly-check-in/(:num)', 'App\Controllers\Admin\GateKeeper::process_check_in/$1', [
        'as' => 'bv_process_check_in'
    ]);

    $routes->add('xu-ly-check-out/(:num)/(:num)', 'App\Controllers\Admin\GateKeeper::process_checkout_check_info/$1/$2', [
        'as' => 'bv_process_check_out'
    ]);
    $routes->add('xu-ly-check-out/(:num)', 'App\Controllers\Admin\GateKeeper::process_checkout_check_info/$1', [
        'as' => 'bv_process_check_out_1'
    ]);
    $routes->add('xu-ly-check-out', 'App\Controllers\Admin\GateKeeper::process_checkout_check_info', [
        'as' => 'bv_process_check_out_2'
    ]);


});

$routes->group('cli', function (RouteCollection $routes) {

});

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

$routes->add('/cart', 'App\Controllers\ShoppingCart::index', ['as' => 'cart']);
$routes->add('/shopping-cart/add', 'App\Controllers\ShoppingCart::add', ['as' => 'cart_add']);
$routes->add('/shopping-cart/decrement', 'App\Controllers\ShoppingCart::decrement', ['as' => 'cart_decrement']);
$routes->add('/shopping-cart/remove', 'App\Controllers\ShoppingCart::remove', ['as' => 'cart_remove']);
$routes->add('/checkout', 'App\Controllers\ShoppingCart::checkout', ['as' => 'cart_checkout']);
$routes->add('/tim-kiem', 'App\Controllers\Home::search', ['as' => 'home_search']);
$routes->add('/lien-he', 'App\Controllers\Home::contact', ['as' => 'home_contact']);

$routes->add('/dang-ky-bao-gia', 'App\Controllers\Home::register', ['as' => 'home_register']);

//$routes->add('/mau-nha-dep', 'App\Controllers\Project::index', ['as' => 'project']);
$routes->add('/cua-hang', 'App\Controllers\Product::category', ['as' => 'product']);
$routes->add('/kinh-nghiem-hay', 'App\Controllers\News::index', ['as' => 'news']);

//$routes->add('/vote-category', 'App\Controllers\Category::insert_votes_rate_category', ['as' => 'insert_votes_rate_category']);
//$routes->add('/vote-category', 'App\Controllers\Category::insert_votes_rate_category', ['as' => 'insert_votes_rate_category']);


$request = Services::request();
$slug = $request->uri->getSegment(1);
if ($slug && $slug !== ADMIN_PATH) {
    if (($config = RouterUrlModel::findBySlug($slug)) !== null) {
        $routes->add('/(:any)', 'App\Controllers\\' . $config->frontend_router . '/' . $config->object_id);
    } else {
//        $routes->add('/(:any)', 'App\Controllers\Error::code404');
    }
}

//route các màn hình thông báo độc lập
$routes->add('thong-bao-xe-ra/(:num)', 'App\Controllers\Admin\GateKeeper::checkout_alert/$1', [
    'as' => 'bv_process_check_out_alert'
]);

$routes->add('thong-bao-nhan-ticket/(:num)', 'App\Controllers\Admin\Operator::take_ticket_alert/$1', [
    'as' => 'nv_process_take_ticket_alert'
]);

$routes->add('thong-bao-lay-hang/(:num)/(:num)', 'App\Controllers\Admin\Operator::take_item_alert/$1/$2', [
    'as' => 'nv_process_take_item_alert'
]);

$routes->add('thong-bao-lay-hang-am-thanh/(:num)/(:num)', 'App\Controllers\Admin\Operator::take_item_alert_voice/$1/$2', [
    'as' => 'nv_process_take_item_alert_voice'
]);
/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
