"use strict";

var EditorCarousel = {
  body: $("body"),
  form: null,
  init: function () {
    if (this.form) return this;

    this.body.append(
      '<form action="/admin/media/carousel" method="post" ' +
        'enctype="multipart/form-data" id="img-form">' +
        '<input type="file" name="_gallery[]" accept="image/*" multiple id="_file"/>' +
        "</form>"
    );

    this.form = $("#img-form", this.body);

    return this;
  },
  cleanup: function () {
    this.form && this.form.remove();
    this.form = null;
  },
  select: function () {
    var self = this;

    $("input", this.form)
      .trigger("click")
      .on("change", self.beginUpload.bind(this));

    return new Promise((resolve, reject) => {
      if (!self.form) {
        resolve(null);
      } else {
        self.form.ajaxForm({
          success: resolve,
          error: reject,
        });
      }
    });
  },
  beginUpload: function (e) {
    $(this.form).trigger("submit");
    this.cleanup();
  },
  register: async function () {
    var res = await EditorCarousel.init().select();

    if (res && res.length > 0) {
      var html = [];
      for (var i = 0; i < res.length; i++) {
        html.push(
          '<li data-src="' + res[i].src + '" data-thumb="' + res[i].src + '">'
        );
        html.push('<div class="img-wrap">');
        html.push('<img src="' + res[i].src + '"/>');
        html.push("</div>");
        html.push("</li>");
      }
      this.html.insert('<ul class="e-carousel">' + html.join("\n") + "</ul>");
    }
  },
};

FroalaEditor.DefineIcon("carousel", { NAME: "carousel", SVG_KEY: "upload" });
FroalaEditor.RegisterCommand("carousel", {
  title: "Chèn thư viện ảnh",
  focus: true,
  undo: true,
  refreshAfterCallback: true,
  callback: EditorCarousel.register,
});
FroalaEditor.RegisterQuickInsertButton("carousel", {
  icon: "carousel",
  title: "Chèn thư viện ảnh",
  callback: EditorCarousel.register,
  undo: true,
});

var editorConfig = {
  charCounterCount: false,
  heightMin: 240,
  language: "vi",
  toolbarButtons: {
    moreText: {
      buttons: [
        "bold",
        "italic",
        "underline",
        "strikeThrough",
        "subscript",
        "superscript",
        "fontFamily",
        "fontSize",
        "textColor",
        "backgroundColor",
        "inlineClass",
        "inlineStyle",
        "clearFormatting",
      ],
    },
    moreParagraph: {
      buttons: [
        "alignLeft",
        "alignCenter",
        "formatOLSimple",
        "alignRight",
        "alignJustify",
        "formatOL",
        "formatUL",
        "paragraphFormat",
        "paragraphStyle",
        "lineHeight",
        "outdent",
        "indent",
        "quote",
      ],
    },
    moreRich: {
      buttons: [
        "insertLink",
        "insertImage",
        "insertVideo",
        "insertTable",
        "carousel",
      ],
      buttonsVisible: 5,
    },
    moreMisc: {
      buttons: [
        "undo",
        "redo",
        "fullscreen",
        "print",
        "getPDF",
        "spellChecker",
        "selectAll",
        "html",
        "help",
      ],
      align: "right",
      buttonsVisible: 2,
    },
  },
  quickInsertButtons: ["image", "table", "ol", "ul", "carousel"],
  imageUploadURL: "/admin/media/upload",
  imageUploadParam: "image",
};

(function ($, body) {
  let heightMin;
  $(".content-js:visible").each(function (i, o) {
    heightMin = $(this).data("height");
    if (heightMin) {
      editorConfig.heightMin = parseInt(heightMin);
    }
    new FroalaEditor("#" + $(this).attr("id"), editorConfig);
  });

  var $form, method, action, o, _prompt;
  $('[data-method="post"]').each(function () {
    $(this).on("click", function () {
      o = $(this);
      _prompt = o.data("prompt");
      if (_prompt && !!_prompt.length) {
        Swal.fire({
          title: _prompt,
          text: "",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Đồng ý",
          cancelButtonText: "Không",
        }).then((result) => {
          if (result.isConfirmed) {
            method = o.data("method");
            action = o.data("action") || o.attr("href");
            $form = $("<form/>", { method: method, action: o.attr("href") });
            $form.append(
              $("<input/>", { name: "_method", value: method, type: "hidden" })
            );
            $form.hide().appendTo("body");
            $form.trigger("submit");
          }
        });
      }
      return !(_prompt && !!_prompt.length);
    });
  });
  $('[data-method="get"]').each(function () {
    $(this).on("click", function () {
      o = $(this);
      _prompt = o.data("prompt");
      action = o.data("action") || o.attr("href");

      if (!_prompt || !_prompt.length) return true;

      return confirm(_prompt);
    });
  });

  $("#main-modal").on("show.bs.modal", function (e) {
    var link = $(e.relatedTarget);
    if (!link || !link.length) return;
    var md = $(this);
    $(".modal-content", md).load(link.attr("href"), function () {
      $('input[name="meta_title"]:visible', md).focus();
      $("form", md).ajaxForm({
        success: function () {
          $.notify(
            { icon: "add_alert", message: "Cập nhật thành công" },
            { type: "success", placement: { from: "bottom", align: "right" } }
          );
        },
        error: function (err) {
          $.notify(
            {
              icon: "add_alert",
              message: err.statusText || "Đã có lỗi xảy ra, hãy thử lại!",
            },
            { type: "danger", placement: { from: "bottom", align: "right" } }
          );
        },
      });
    });
  });
  console.log(1);
  // $('.datetimepicker').datetimepicker({
  //     format: 'YYYY-MM-DD',
  // });
  // $('#datetimepicker1').datetimepicker();
  jQuery(".datetimepicker").datetimepicker({
    format: "d-m-Y H:i:s",
  });

  jQuery(".datepicker").datetimepicker({
    format: "d-m-Y",
  });
  if ($("#videoElement").length > 0) {
    change_play_cam($("#select_camera_in"), "videoElement");
  }
})(jQuery, jQuery("body"));

//# sourceMappingURL=main.js.map

var flvPlayer;

function change_play_cam(list_cam, object_video_id) {
  var link_play = $(list_cam).val();

  if (flvjs.isSupported()) {
    if (flvPlayer) {
      flvPlayer.destroy();
    }
    var videoElement = document.getElementById(object_video_id);
    if (videoElement) {
      flvPlayer = flvjs.createPlayer({
        type: "flv",
        url: link_play,
      });
      flvPlayer.attachMediaElement(videoElement);
      flvPlayer.load();
      flvPlayer.muted = true;
      flvPlayer.play();
    }
  }
}

var player_in;
var player_out;

function change_play_cam_gate_in(list_cam, object_video_id) {
  var link_play = $(list_cam).val();

  if (flvjs.isSupported()) {
    if (player_in) {
      player_in.destroy();
    }
    var videoElement = document.getElementById(object_video_id);
    if (videoElement) {
      player_in = flvjs.createPlayer({
        type: "flv",
        url: link_play,
      });
      player_in.isLive = true;
      player_in.attachMediaElement(videoElement);
      player_in.load();
      player_in.muted = true;
      player_in.play();
    }
  }
}

function change_play_cam_gate_out(list_cam, object_video_id) {
  var link_play = $(list_cam).val();

  if (flvjs.isSupported()) {
    if (player_out) {
      player_out.destroy();
    }
    var videoElement = document.getElementById(object_video_id);
    if (videoElement) {
      player_out = flvjs.createPlayer({
        type: "flv",
        url: link_play,
      });
      player_out.isLive = true;
      player_out.attachMediaElement(videoElement);
      player_out.load();
      player_out.muted = true;
      player_out.play();
    }
  }
}

function change_check_cam_in() {
  var cam_in_status = $("#check_cam_in").is(":checked");
  var cam_out_status = $("#check_cam_out").is(":checked");
  if (!cam_in_status && !cam_out_status) {
    $("#check_cam_in").prop("checked", true);
    return;
  }
  if (cam_in_status && cam_out_status) {
    $(".div_camera_in").removeClass("hidden");
    $(".div_camera_out").removeClass("hidden");

    $(".div_camera_in").addClass("col-md-6");
    $(".div_camera_out").addClass("col-md-6");

    $(".div_camera_in").removeClass("col-md-12");
    $(".div_camera_out").removeClass("col-md-12");

    $("#videoElement_in").attr("width", "100%");
    $("#videoElement_out").attr("width", "100%");
  }
  if (!cam_in_status && cam_out_status) {
    $(".div_camera_in").addClass("hidden");

    $(".div_camera_out").removeClass("hidden");
    $(".div_camera_out").addClass("col-md-12");
    $(".div_camera_out").removeClass("col-md-6");

    $("#videoElement_out").attr("width", "75%");
  }
  if (cam_in_status && !cam_out_status) {
    $(".div_camera_out").addClass("hidden");

    $(".div_camera_in").removeClass("hidden");
    $(".div_camera_in").addClass("col-md-12");
    $(".div_camera_in").removeClass("col-md-6");

    $("#videoElement_in").attr("width", "75%");
  }
  // var check_cam_out_status = $()
}

function change_check_cam_out() {
  var cam_in_status = $("#check_cam_in").is(":checked");
  var cam_out_status = $("#check_cam_out").is(":checked");
  if (!cam_in_status && !cam_out_status) {
    $("#check_cam_out").prop("checked", true);
    return;
  }

  console.log(cam_in_status, cam_out_status);

  if (cam_in_status && cam_out_status) {
    console.log(1);

    $(".div_camera_in").removeClass("hidden");
    $(".div_camera_out").removeClass("hidden");

    $(".div_camera_in").addClass("col-md-6");
    $(".div_camera_out").addClass("col-md-6");

    $(".div_camera_in").removeClass("col-md-12");
    $(".div_camera_out").removeClass("col-md-12");

    $("#videoElement_in").attr("width", "100%");
    $("#videoElement_out").attr("width", "100%");
  }

  if (!cam_in_status && cam_out_status) {
    console.log(2);

    $(".div_camera_in").addClass("hidden");

    $(".div_camera_out").removeClass("hidden");
    $(".div_camera_out").addClass("col-md-12");
    $(".div_camera_out").removeClass("col-md-6");

    $("#videoElement_out").attr("width", "75%");
  }

  if (cam_in_status && !cam_out_status) {
    console.log(3);
    $(".div_camera_out").addClass("hidden");
    $(".div_camera_in").removeClass("hidden");

    $(".div_camera_in").addClass("col-md-12");
    $(".div_camera_in").removeClass("col-md-6");
    $("#videoElement_in").attr("width", "75%");
  }
  // var check_cam_out_status = $()
}

function gate_keeper_checkin(object_video_id) {
  $("#btn_check_in").prop("disabled", true);
  var canvas = document.getElementById("canvas_" + object_video_id);
  var video = document.getElementById(object_video_id);
  var videoWidth = video.videoWidth;
  var videoHeight = video.videoHeight;
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  var context = canvas.getContext("2d");
  context.fillRect(0, 0, videoWidth, videoHeight);
  context.drawImage(video, 0, 0, videoWidth, videoHeight);
  var _image_base64 = canvas.toDataURL("image/jpeg", 1);
  var _data = {
    image_base64: _image_base64,
  };
  $.post("/Admin/GateKeeper/insert_check_in_image", _data, function (res) {
    if (res) {
      $("#btn_check_in").prop("disabled", false);
      window.location.href = "/quantri/xu-ly-check-in/" + res;
    }
  });
}

function snap_video(object_video_id, image_display_id, input_base46_id) {
  var canvas = document.getElementById("canvas_" + object_video_id);
  var video = document.getElementById(object_video_id);
  var videoWidth = video.videoWidth;
  var videoHeight = video.videoHeight;
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  var context = canvas.getContext("2d");
  context.fillRect(0, 0, videoWidth, videoHeight);
  context.drawImage(video, 0, 0, videoWidth, videoHeight);
  $("#" + input_base46_id).val(canvas.toDataURL("image/jpeg", 1));
  $("#" + image_display_id).attr("src", canvas.toDataURL("image/jpeg", 1));
}

function qr_value_key_code(event, id) {
  var key_code = event.keyCode;
  if (key_code == "13") {
    process_smo_info($("#qr_value"), id);
  }
}

function process_smo_info(qr_code_input, id) {
  var _qr_code = $(qr_code_input).val().trim();
  if (_qr_code) {
    $("#qr_value").attr("disabled", true);
    var list_qr_code = JSON.parse(localStorage.getItem("list_qr_code"));
    if (!list_qr_code) {
      list_qr_code = new Array();
    }
    for (var i = 0; i < list_qr_code.length; i++) {
      if (_qr_code == list_qr_code[i].qr_code) {
        $.notify(
          { icon: "add_alert", message: "QR Code trùng lặp" },
          {
            type: "warning",
            placement: { from: "bottom", align: "right" },
          }
        );
        $("#qr_value").attr("disabled", false);
        $("#qr_value").val("");
        return;
      }
    }
    var _data = {
      qr_code: _qr_code,
      in_out_id: id,
    };
    $.post("/Admin/GateKeeper/get_smo_info", _data, function (res) {
      var result = JSON.parse(res);
      if (result == "-1") {
        $.notify(
          {
            icon: "add_alert",
            message: "QR code đã được sử dụng",
          },
          { type: "danger", placement: { from: "bottom", align: "right" } }
        );
        $("#qr_value").attr("disabled", false);
        $("#qr_value").val("");
        return;
      }
      if (result == "-2") {
        $.notify(
          {
            icon: "add_alert",
            message: "Không tìm thấy thông tin mã QR",
          },
          { type: "warning", placement: { from: "bottom", align: "right" } }
        );
        $("#qr_value").attr("disabled", false);
        $("#qr_value").val("");
        return;
      }
      if (res) {
        console.log(result);
        var order_info = result.order_info;
        if (!order_info) {
          $.notify(
            { icon: "add_alert", message: "Không tìm thấy thông tin mã QR" },
            {
              type: "warning",
              placement: { from: "bottom", align: "right" },
            }
          );
        }

        var save_item = {};
        save_item.qr_code = _qr_code;
        save_item.data = result;

        for (var i = 0; i < list_qr_code.length; i++) {
          if (save_item.data.order_info && list_qr_code[i].data.order_info) {
            if (
              save_item.data.order_info.VehicleCode !=
              list_qr_code[i].data.order_info.VehicleCode
            ) {
              $.notify(
                {
                  icon: "add_alert",
                  message: "Không trùng biển số với lệnh trước",
                },
                {
                  type: "warning",
                  placement: { from: "bottom", align: "right" },
                }
              );
              break;
            }
          }
        }

        list_qr_code.push(save_item);
        localStorage.setItem("list_qr_code", JSON.stringify(list_qr_code));

        process_check_qr_info_car_number();

        process_display_qr_code_info(list_qr_code, id);

        $("#qr_value").attr("disabled", false);
        $("#qr_value").val("");
      } else {
        $.notify(
          {
            icon: "add_alert",
            message: "Không cho phép lấy thông tin từ SMO",
          },
          { type: "danger", placement: { from: "bottom", align: "right" } }
        );
        $("#qr_value").attr("disabled", false);
        $("#qr_value").val("");
      }
    });
  }
}

function delete_smo_info(qr_code, in_out_id) {
  var list_qr_code = JSON.parse(localStorage.getItem("list_qr_code"));
  var modify_list = new Array();
  for (var i = 0; i < list_qr_code.length; i++) {
    if (list_qr_code[i].qr_code != qr_code) {
      modify_list.push(list_qr_code[i]);
    }
  }

  localStorage.setItem("list_qr_code", JSON.stringify(modify_list));
  var _data = {
    qr_code: qr_code,
    in_out_id: in_out_id,
  };
  $.post("/Admin/GateKeeper/delete_smo_info", _data, function (res) {});

  process_display_qr_code_info(modify_list, in_out_id);
  process_check_qr_info_car_number();
}

function process_display_qr_code_info(list_qr_code, in_out_id) {
  $("#table_check_in tr[tr_qr_code]").remove();

  var template =
    ' <tr tr_qr_code="{qr_code}">' +
    " <td><h6>{title}</h6></td>" +
    '<td class="text-center td-name">' +
    '<button onclick="show_order_detai(\'{qr_code}\')" class="btn btn-social btn-fill btn-info btn-round" >' +
    ' <i class="material-icons">qr_code</i> ' +
    "{qr_code}</button>" +
    "</td>" +
    '<td class="text-center">' +
    "<div >" +
    "<h4 h4_driver_name >{driver_name}</h4>" +
    "</div>" +
    "</td>" +
    ' <td class="text-center">' +
    "<div >" +
    '<h4 h4_car_number data-qr_code="{qr_code}" >{car_number}</h4>' +
    "</div>" +
    "</td>" +
    '<td id="check_car_number" class="text-center">' +
    '<button class="btn {btn_type} btn-sm btn-round" title="{btn_title}">' +
    '<i class="material-icons">{icon_check}</i>' +
    "</button>" +
    "</td>" +
    '<td class="text-center">' +
    "<button onclick=\"delete_smo_info('{qr_code}'," +
    in_out_id +
    ')" class="btn btn-danger btn-sm btn-round" title="Xóa">' +
    '<i class="material-icons">delete_forever</i>' +
    "</button>" +
    "</td>" +
    "</tr>";
  for (var i = 0; i < list_qr_code.length; i++) {
    var _qr_code = list_qr_code[i].qr_code;
    var order_info = list_qr_code[i].data.order_info;
    var title_qr = "";
    if (i == 0) {
      title_qr = "Mã QR";
    }
    if (order_info) {
      var row_add = template.replaceAll("{title}", title_qr);
      row_add = row_add.replaceAll("{qr_code}", _qr_code);
      row_add = row_add.replaceAll("{car_number}", order_info.VehicleCode);
      var _dr_name =
        order_info.OicPBatch +
        (order_info.OicPtrip ? " " + order_info.OicPtrip : "");
      row_add = row_add.replaceAll("{driver_name}", _dr_name.trim());
      row_add = row_add.replaceAll("{btn_type}", "btn-success");
      row_add = row_add.replaceAll("{btn_title}", "Khớp");
      row_add = row_add.replaceAll("{icon_check}", "done");

      $("#table_check_in tr:last").after(row_add);
    } else {
      var row_add = template.replaceAll("{title}", title_qr);
      row_add = row_add.replaceAll("{qr_code}", _qr_code);
      row_add = row_add.replaceAll("{car_number}", "");
      row_add = row_add.replaceAll("{driver_name}", "");
      row_add = row_add.replaceAll("{btn_type}", "btn-danger");
      row_add = row_add.replaceAll("{btn_title}", "Không khớp");
      row_add = row_add.replaceAll("{icon_check}", "help_center");
      $("#table_check_in tr:last").after(row_add);
    }
  }
}

function process_check_qr_info_car_number() {
  var list_qr_code = JSON.parse(localStorage.getItem("list_qr_code"));
  var car_number_detected = $("#h4_car_number_detected").html();
  if (
    car_number_detected &&
    list_qr_code.length &&
    list_qr_code[0].data.order_info &&
    car_number_detected == list_qr_code[0].data.order_info.VehicleCode
  ) {
    $("#check_car_number").html(
      '<button class="btn btn-success btn-sm btn-round" title="Khớp">' +
        ' <i class="material-icons">done</i>' +
        "</button>"
    );
    return;
  }
  $("#check_car_number").html(
    '<button class="btn btn-danger btn-sm btn-round" title="Không khớp">' +
      '   <i class="material-icons">help_center</i>' +
      "</button>"
  );
}

function show_order_detai(qr_code) {
  $("#tbody_smo_order_detail").html("");
  var list_qr_code = JSON.parse(localStorage.getItem("list_qr_code"));
  var _html = "";
  for (var i = 0; i < list_qr_code.length; i++) {
    if (list_qr_code[i].qr_code == qr_code && list_qr_code[i].data.order_info) {
      for (var k = 0; k < list_qr_code[i].data.order_detail.length; k++) {
        var order_detail = list_qr_code[i].data.order_detail[k];

        _html += "<tr>";
        _html += '<td class="text-left">' + order_detail.MaterialCode + "</td>";
        _html += '<td class="text-left">' + order_detail.MaterialText + "</td>";
        _html += '<td class="text-center">' + order_detail.Quantity + "</td>";
        _html += "</tr>";
      }
      $("#tbody_smo_order_detail").html(_html);
    }
  }
  $("#qr_info_modal").modal("show");
}

function process_checkin_cancel(id) {
  window.location.href = "/Admin/GateKeeper/process_checkin_cancel/" + id;
}

function process_checkin_reject(id) {
  var _list_qr_code = JSON.parse(localStorage.getItem("list_qr_code"));

  var _data = {
    list_qr_code: _list_qr_code,
  };
  $.post(
    "/Admin/GateKeeper/process_checkin_reject/" + id,
    _data,
    function (res) {
      if (res) {
        window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
        return;
      } else {
        window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
        return;
      }
    }
  );
}

function process_checkin_not_set_order(id) {
  var _list_qr_code = JSON.parse(localStorage.getItem("list_qr_code"));

  var _data = {
    list_qr_code: _list_qr_code,
  };
  $.post(
    "/Admin/GateKeeper/process_checkin_not_set_order/" + id,
    _data,
    function (res) {
      if (res) {
        window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
        return;
      } else {
        window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
        return;
      }
    }
  );
}

function process_checkin_set_order(
  id,
  car_number_detected,
  driver_detected,
  is_exist_car
) {
  var is_verify_car_number = false;
  var is_verify_driver = false;

  var list_qr_code = JSON.parse(localStorage.getItem("list_qr_code"));
  var checkin_car_number_note = $("#checkin_car_number_note").val();
  var checkin_driver_hand = $("#checkin_driver_hand").val();
  if (checkin_car_number_note) {
    is_verify_car_number = true;
  }

  if (car_number_detected && is_exist_car) {
    is_verify_car_number = true;
  }

  if (!driver_detected && checkin_driver_hand) {
    console.log("driver1", driver_detected, checkin_driver_hand);
    is_verify_driver = true;
  }
  if (driver_detected) {
    console.log("driver2", driver_detected, checkin_driver_hand);

    is_verify_driver = true;
  }

  if (!is_verify_car_number) {
    $.notify(
      {
        icon: "add_alert",
        message: "Không cấp số do chưa xác thực được biển số xe",
      },
      {
        type: "danger",
        placement: { from: "bottom", align: "right" },
      }
    );
    return;
  }
  if (!is_verify_driver) {
    $.notify(
      {
        icon: "add_alert",
        message: "Không cấp số do chưa xác thực được tài xế",
      },
      {
        type: "danger",
        placement: { from: "bottom", align: "right" },
      }
    );
    return;
  }

  if (list_qr_code.length > 0) {
    for (var i = 0; i < list_qr_code.length; i++) {
      if (!list_qr_code[i].data.order_info) {
        $.notify(
          {
            icon: "add_alert",
            message: "Không cấp số do tồn tại mã QR không có thông tin",
          },
          { type: "danger", placement: { from: "bottom", align: "right" } }
        );
        return;
      }
    }
    var first_vehicle_code = list_qr_code[0].data.order_info.VehicleCode;
    for (var i = 0; i < list_qr_code.length; i++) {
      if (list_qr_code[i].data.order_info.VehicleCode != first_vehicle_code) {
        $.notify(
          {
            icon: "add_alert",
            message: "Không cấp số do tồn tại mã QR không đồng nhất biển số xe",
          },
          { type: "danger", placement: { from: "bottom", align: "right" } }
        );
        return;
      }
    }
  }

  var _data = {
    list_qr_code: list_qr_code,
  };
  $.post(
    "/Admin/GateKeeper/process_checkin_set_order/" + id,
    _data,
    function (res) {
      res = JSON.parse(res);
      console.log("process_checkin_set_order", res);
      if (res["code"] > 0) {
        // var myWindow = window.open("/Admin/GateKeeper/show_window_checkin_order/"+id, "myWindow", "width=300,height=400");
        var url = "/Admin/GateKeeper/show_window_checkin_order/" + id;
        popupCenter({ url: url, title: "In STT", w: 350, h: 400 });
        $.notify(
          { icon: "add_alert", message: "Đã xử lý cấp số" },
          {
            type: "success",
            placement: { from: "bottom", align: "right" },
          }
        );
        $("#div_check_in")
          .find("input, textarea, button, select")
          .attr("disabled", "disabled");
        window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
        return;
      } else if (res["code"] == "-1") {
        $.notify(
          {
            icon: "add_alert",
            message: "Mã QR code " + res["info"] + " đã sử dụng",
          },
          {
            type: "danger",
            placement: { from: "bottom", align: "right" },
          }
        );
        return;
      } else {
        $.notify(
          { icon: "add_alert", message: "Lỗi cấp số" },
          {
            type: "danger",
            placement: { from: "bottom", align: "right" },
          }
        );
        return;
      }
    }
  );
}

function re_print_order(id) {
  var url = "/Admin/GateKeeper/show_window_checkin_order/" + id;
  popupCenter({ url: url, title: "In STT", w: 350, h: 400 });
}

function process_checkout_check_info(object_video_id) {
  var _camera_id = $("#select_camera_out option:selected").data(
    "camera_out_id"
  );
  $("#btn_check_out").prop("disabled", true);
  var canvas = document.getElementById("canvas_" + object_video_id);
  var video = document.getElementById(object_video_id);
  var videoWidth = video.videoWidth;
  var videoHeight = video.videoHeight;
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
  var context = canvas.getContext("2d");
  context.fillRect(0, 0, videoWidth, videoHeight);
  context.drawImage(video, 0, 0, videoWidth, videoHeight);
  var _image_base64 = canvas.toDataURL("image/jpeg", 1);
  var _data = {
    camera_id: _camera_id,
    image_base64: _image_base64,
  };
  $.post(
    "/Admin/GateKeeper/process_checkout_check_car_number",
    _data,
    function (res) {
      res = JSON.parse(res);
      if (res.in_out_id) {
        window.location.href =
          "/quantri/xu-ly-check-out/" + _camera_id + "/" + res.in_out_id;
      } else {
        window.location.href = "/quantri/xu-ly-check-out/" + _camera_id;
      }
      $("#btn_check_in").prop("disabled", false);
    }
  );
}

function process_checkout_reject(camera_id, in_out_id) {
  if (!in_out_id) {
    window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
    return;
  }
  var _data = {};
  $.post(
    "/Admin/GateKeeper/process_checkout_reject/" + camera_id + "/" + in_out_id,
    _data,
    function (res) {
      res = JSON.parse(res);
      window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
      return;
    }
  );
}

function process_update_note(id) {
console.log("🚀 ~ file: main.js:1392 ~ process_update_note ~ id:", id)
  var note = $("#form_car_number_hand").val();
  var id = $("#process_ticket").val();
  var _data = {note};
  $.post("/Admin/Operator/process_update_note/" + id, _data, function (res) {
    res = JSON.parse(res);
    if (res) {
        window.location.href = '/quantri/nhan-vien-thu-tu-lay-ticket';
        return;
    } else {
        $.notify({ icon: 'add_alert', message: "Không tìm thấy thông tin Ticket" }, {
            type: "danger",
            placement: { from: 'bottom', align: 'right' }
        });
        $('#btn_take_ticket_' + id).prop("disabled", false);
        return;
    }
  });
}
function show_re_check_car_number() {
  $("#form_car_number_hand").val("");
  $("#car_number_hand_modal").modal("show");
}
function pre_print_stt() {
  $("#print_stt").modal("show");
}
function print_stt() {
  var id = $("#print_car_number").val();
  var url = "/Admin/GateKeeper/show_window_pre_print_stt/" + id;
  popupCenter({ url: url, title: "In STT", w: 595, h: 842 });
}
function print_order() {
  $("#print_order").modal("show");
}
function process_print_ticket(id) {
  console.log("🚀 ~ file: main.js:838 ~ re_print_order ~ id:", id)
  var solenh = $("#print_ticket_order").val();
  console.log("🚀 ~ file: main.js:838 ~ solenh ~ solenh:", solenh)
  var url = "/Admin/Operator/show_window_pre_print_ticket/" + solenh;
  popupCenter({ url: url, title: "In STT", w: 595, h: 842 });
}

function re_check_car_number_info(camera_id) {
  var _car_number = $("#form_car_number_hand").val();
  if (!_car_number) {
    $.notify(
      {
        icon: "add_alert",
        message: "Cần nhập biển số xe để tiến hành kiểm tra lại",
      },
      {
        type: "warning",
        placement: { from: "bottom", align: "right" },
      }
    );
    return;
  }
  var _data = {
    car_number: _car_number,
  };
  $.post(
    "/Admin/GateKeeper/process_checkout_recheck_car_number_info",
    _data,
    function (res) {
      res = JSON.parse(res);
      console.log("🚀 ~ file: main.js:977 ~ res:", res);
      if (res) {
        window.location.href =
          "/quantri/xu-ly-check-out/" + camera_id + "/" + res;
        return;
      } else {
        // $.notify({
        //     icon: 'add_alert',
        //     message: "Không tìm thấy thông tin biển số xe " + _car_number
        // }, {type: "warning", placement: {from: 'bottom', align: 'right'}});
        // return;
        Swal.fire({
          title: "Bạn có chắc muốn cho xe ra hay không?",
          text: "Không tìm thấy lịch sử xe vào",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Đồng ý",
          cancelButtonText: "Không",
        }).then((result) => {
          if (result.isConfirmed) {
            window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
            return;
          }
        });
      }
    }
  );
}

function process_checkout_allow(
  checkout_ai_car_number,
  area_id,
  camera_id,
  checkout_order_status,
  in_out_id
) {
  if (!in_out_id) {
    Swal.fire({
      title: "Bạn có chắc muốn cho xe ra hay không?",
      text: "Không tìm thấy lịch sử xe vào",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Đồng ý",
      cancelButtonText: "Không",
    }).then((result) => {
      if (result.isConfirmed) {
        window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
        return;
      }
    });
    return;
  }
  if (!checkout_order_status) {
    $("#form_checkout_note_hand").val("");
    $("#form_checkout_note_hand").focus();
    $("#form_checkout_note_hand").focus();

    $("#form_checkout_by_hand_modal").modal("show");
    return;
  }

  var _data = {
    camera_id: camera_id,
    in_out_id: in_out_id,
    checkout_ai_car_number: checkout_ai_car_number,
    area_id: area_id,
  };
  $.post("/Admin/GateKeeper/process_checkout_done", _data, function (res) {
    res = JSON.parse(res);
    if (res) {
      window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
      return;
    } else {
      $.notify(
        { icon: "add_alert", message: "Có lỗi trong việc xử lý cho ra" },
        {
          type: "warning",
          placement: { from: "bottom", align: "right" },
        }
      );
      return;
    }
  });
}

function checkout_by_hand(car_number, area_id, camera_id, in_out_id) {
  var checkout_note = $("#form_checkout_note_hand").val();
  if (!checkout_note) {
    $.notify(
      { icon: "add_alert", message: "Vui lòng nhập lý do, ghi chú" },
      {
        type: "danger",
        placement: { from: "bottom", align: "right" },
      }
    );
    return;
  }
  var _data = {
    camera_id: camera_id,
    in_out_id: in_out_id,
    checkout_note: checkout_note,
    checkout_ai_car_number: car_number,
    area_id: area_id,
  };
  $.post("/Admin/GateKeeper/process_checkout_done", _data, function (res) {
    res = JSON.parse(res);
    if (res) {
      window.location.href = "/quantri/bao-ve-kiem-soat-vao-ra";
      return;
    } else {
      $.notify(
        { icon: "add_alert", message: "Có lỗi trong việc xử lý cho ra" },
        {
          type: "warning",
          placement: { from: "bottom", align: "right" },
        }
      );
      return;
    }
  });
}

const popupCenter = ({ url, title, w, h }) => {
  // Fixes dual-screen position                             Most browsers      Firefox
  const dualScreenLeft =
    window.screenLeft !== undefined ? window.screenLeft : window.screenX;
  const dualScreenTop =
    window.screenTop !== undefined ? window.screenTop : window.screenY;

  const width = window.innerWidth
    ? window.innerWidth
    : document.documentElement.clientWidth
    ? document.documentElement.clientWidth
    : screen.width;
  const height = window.innerHeight
    ? window.innerHeight
    : document.documentElement.clientHeight
    ? document.documentElement.clientHeight
    : screen.height;

  const systemZoom = width / window.screen.availWidth;
  const left = (width - w) / 2 / systemZoom + dualScreenLeft;
  const top = (height - h) / 2 / systemZoom + dualScreenTop;
  const newWindow = window.open(
    url,
    title,
    `
      scrollbars=yes,
      width=${w / systemZoom}, 
      height=${h / systemZoom}, 
      top=${top}, 
      left=${left}
      `
  );

  if (window.focus) newWindow.focus();
};

function show_car_number_note(car_number_ai_detected) {
  $("#form_car_number_hand").val("");
  $("#form_car_number_note").val("");

  var car_number_hand = $("#checkin_car_number_hand").val();
  var car_number_note = $("#checkin_car_number_note").val();

  console.log(car_number_note);
  // var _list_qr_code = JSON.parse(localStorage.getItem("list_qr_code"));
  // if(_list_qr_code.length > 0){
  //     $('#div_for_khach_le').addClass('hidden');
  // }else {
  //     $('#div_for_khach_le').removeClass('hidden');
  // }
  $("#form_car_number_hand").val(car_number_hand);
  $("#form_car_number_note").val(car_number_note);
  if (car_number_hand || car_number_note) {
    $("#check_xu_ly_tay_bien_so").prop("checked", true);
  } else {
    $("#check_xu_ly_tay_bien_so").prop("checked", false);
  }
  $("#car_number_hand_modal").modal("show");
}

function update_car_number_note(id) {
  var customer_type = $('input[name="customer_type"]:checked').val();

  var car_number_hand = $("#form_car_number_hand").val();
  var car_number_note = $("#form_car_number_note").val();

  var list_qr_code = JSON.parse(localStorage.getItem("list_qr_code"));
  if (customer_type == "smo" && list_qr_code.length == 0) {
    $("#form_car_number_note").val("");
    $.notify(
      { icon: "add_alert", message: "Chưa có mã QR nào được nhập" },
      {
        type: "warning",
        placement: { from: "bottom", align: "right" },
      }
    );
    return;
  }

  if (customer_type == "not_smo" && !car_number_hand) {
    $.notify(
      { icon: "add_alert", message: "Biển số cần phải nhập" },
      {
        type: "warning",
        placement: { from: "bottom", align: "right" },
      }
    );

    return;
  }
  if (!car_number_note) {
    $.notify(
      { icon: "add_alert", message: "Lý do/ ghi chú cần phải nhập" },
      {
        type: "warning",
        placement: { from: "bottom", align: "right" },
      }
    );

    return;
  }

  var _data = {
    checkin_car_number_hand: car_number_hand,
    checkin_car_number_note: car_number_note,
  };
  $.post(
    "/Admin/GateKeeper/save_car_number_note_info/" + id,
    _data,
    function (res) {
      $("#checkin_car_number_hand").val(car_number_hand);
      $("#checkin_car_number_note").val(car_number_note);
      if (car_number_hand || car_number_note) {
        $("#check_xu_ly_tay_bien_so").prop("checked", true);
      } else {
        $("#check_xu_ly_tay_bien_so").prop("checked", false);
      }
      $("#car_number_hand_modal").modal("hide");
    }
  );
}

function show_driver_note(driver_ai_detected) {
  $("#form_driver_hand").val("");
  $("#form_driver_note").val("");

  var driver_hand = $("#checkin_driver_hand").val();
  var driver_note = $("#checkin_driver_note").val();

  $("#form_driver_hand").val(driver_hand);
  $("#form_driver_note").val(driver_note);
  if (driver_hand || driver_note) {
    $("#check_xu_ly_tay_tai_xe").prop("checked", true);
  } else {
    $("#check_xu_ly_tay_tai_xe").prop("checked", false);
  }
  $("#driver_hand_modal").modal("show");
}

function update_driver_note(id) {
  var driver_hand = $("#form_driver_hand").val();
  var driver_note = $("#form_driver_note").val();

  if (!driver_hand) {
    $.notify(
      { icon: "add_alert", message: "Tên tài xế cần phải nhập" },
      {
        type: "warning",
        placement: { from: "bottom", align: "right" },
      }
    );

    return;
  }
  if (!driver_note) {
    $.notify(
      { icon: "add_alert", message: "Lý do/ ghi chú cần phải nhập" },
      {
        type: "warning",
        placement: { from: "bottom", align: "right" },
      }
    );

    return;
  }

  var _data = {
    checkin_driver_hand: driver_hand,
    checkin_driver_note: driver_note,
  };
  $.post(
    "/Admin/GateKeeper/save_driver_note_info/" + id,
    _data,
    function (res) {
      $("#checkin_driver_hand").val(driver_hand);
      $("#checkin_driver_note").val(driver_note);
      if (driver_hand || driver_note) {
        $("#check_xu_ly_tay_tai_xe").prop("checked", true);
      } else {
        $("#check_xu_ly_tay_tai_xe").prop("checked", false);
      }
      $("#driver_hand_modal").modal("hide");
    }
  );
}

function process_customer_type() {
  var customer_type = $('input[name="customer_type"]:checked').val();
  console.log(customer_type);
  if (customer_type == "smo") {
    $("#div_for_khach_le").addClass("hidden");
    $("#form_car_number_hand").val("");
    $("#form_car_number_note").val("");
  } else {
    $("#div_for_khach_le").removeClass("hidden");
    var car_number_detect = $("#h4_car_number_detected").text();
    // $('#form_car_number_hand').val('');
    // $('#form_car_number_note').val('');
    if (!$("#form_car_number_hand").val()) {
      $("#form_car_number_hand").val(car_number_detect);
    }
  }
}

function process_register_car(id, car_numer_detected) {
  window.location.href = "/quantri/car/create/" + id + "/" + car_numer_detected;
}

function process_register_driver(id) {
  window.location.href = "/quantri/driver/create/" + id;
}

function system_throad() {
  var system_id = $("#pump_system_id").val();
  $.get("/admin/Operator/select_throad/" + system_id, function (data) {
    // alert("Data: " + data + "\nStatus: " + status);
    let result = JSON.parse(data);
    console.log(result);

    let _html = '<option value="">Chọn họng</option>';
    for (let i = 0; i < result.length; i++) {
      if (result[i].active == "1") {
        _html +=
          '<option  value="' +
          result[i].id +
          '">' +
          result[i].throad_name +
          "</option>";
      }
    }
    $("#thread_pararell_id").html(_html);
  });
}

function system_throad_update($id) {
  var system_id = $("#update_pump_system_id").val();
  var item = $id;
  $.get("/admin/Operator/select_throad_update/" + system_id, function (data) {
    // alert("Data: " + data + "\nStatus: " + status);
    let result = JSON.parse(data);
    let _html = '<option value="">Chọn họng</option>';
    for (let i = 0; i < result.length; i++) {
      if (result[i].id != item && result[i].active == "1") {
        _html +=
          '<option  value="' +
          result[i].id +
          '">' +
          result[i].throad_name +
          "</option>";
      }
    }
    $("#update_thread_pararell_id").html(_html);
  });
}

function process_take_ticket(id) {
  console.log("🚀 ~ file: main.js:1346 ~ process_take_ticket ~ id:", id)
  $("#btn_take_ticket_" + id).prop("disabled", true);
  var _data = {};
  $.post("/Admin/Operator/process_get_ticket/" + id, _data, function (res) {
    res = JSON.parse(res);
    if (res == 0) {
      $("#create_note_modal").modal("show");
      $("#process_ticket").val(id);
    }
    if (res) {
      window.location.href = "/quantri/nhan-vien-thu-tu-lay-ticket";
      return;
    } else {
      $.notify(
        { icon: "add_alert", message: "Không tìm thấy thông tin Ticket" },
        {
          type: "danger",
          placement: { from: "bottom", align: "right" },
        }
      );
      $("#btn_take_ticket_" + id).prop("disabled", false);
      return;
    }
    // if (res == -1) {
    //     $.notify({ icon: 'add_alert', message: "Không tìm thấy họng bơm phù hợp với hàng hóa, vui lòng xem lại cấu hình" }, {
    //         type: "danger",
    //         placement: { from: 'bottom', align: 'right' }
    //     });
    //     $('#btn_take_ticket_' + id).prop("disabled", false);
    //     return;
    // }

    // if (res) {
    //     window.location.href = '/quantri/nhan-vien-thu-tu-lay-ticket';
    //     return;
    // } else {
    //     $.notify({ icon: 'add_alert', message: "Không tìm thấy thông tin Ticket" }, {
    //         type: "danger",
    //         placement: { from: 'bottom', align: 'right' }
    //     });
    //     $('#btn_take_ticket_' + id).prop("disabled", false);
    //     return;
    // }
  });
}
function process_alert_get_ticket(id) {
  console.log("🚀 ~ file: main.js:1217 ~ process_alert_get_ticket ~ id:", id);
  // $('#create_note_modal').modal('show');
  var _data = {};
  // $('#btn_take_ticket_' + id).prop("disabled", true);
  // var _data = {
  //     id,
  //     area_id,
  //     checkin_time
  // }
  console.log(
    "🚀 ~ file: main.js:1221 ~ process_alert_get_ticket ~ _data:",
    _data
  );

  $.post(
    "/Admin/Operator/process_alert_get_ticket/" + id,
    _data,
    function (res) {
      res = JSON.parse(res);
      console.log("🚀 ~ file: main.js:1229 ~ res:", res);
      // if (res == -1) {
      //     $.notify({ icon: 'add_alert', message: "Không tìm thấy họng bơm phù hợp với hàng hóa, vui lòng xem lại cấu hình" }, {
      //         type: "danger",
      //         placement: { from: 'bottom', align: 'right' }
      //     });
      //     $('#btn_take_ticket_' + id).prop("disabled", false);
      //     return;
      // }

      // if (res) {
      //     window.location.href = '/quantri/nhan-vien-thu-tu-lay-ticket';
      //     return;
      // } else {
      //     $.notify({ icon: 'add_alert', message: "Không tìm thấy thông tin Ticket" }, {
      //         type: "danger",
      //         placement: { from: 'bottom', align: 'right' }
      //     });
      //     $('#btn_take_ticket_' + id).prop("disabled", false);
      //     return;
      // }
    }
  );
}

function process_reject_get_ticket(id) {
  Swal.fire({
    title: "Bạn có chắc muốn hủy bản ghi này không?",
    // text: "Nếu hủy thì tài xế phải thực hiện checkin lại từ đầu",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Đồng ý",
    cancelButtonText: "Không",
  }).then((result) => {
    if (result.isConfirmed) {
      window.location.href = "/Admin/Operator/process_reject_get_ticket/" + id;
      return;
    }
  });
}
$(function () {
  $("#area-select").on("change", function (e) {
    let area_id = $(this).val();
    load_camera_by_area(area_id);
  });

  function load_camera_by_area(area_id) {
    $.get("danh-sach-carmera/?id=" + area_id, function (data) {
      let result = JSON.parse(data);
      let link_play_cam_in = result.camera_in;
      let link_play_cam_out = result.camera_out;
      let _html_cam_in = "";
      let _html_cam_out = "";
      for (let i = 0; i < link_play_cam_in.length; i++) {
        _html_cam_in +=
          '<option  value="' +
          link_play_cam_in[i].link_play +
          '">' +
          link_play_cam_in[i].title +
          "</option>";
      }

      for (let i = 0; i < link_play_cam_out.length; i++) {
        _html_cam_out +=
          '<option  value="' +
          link_play_cam_out[i].link_play +
          '">' +
          link_play_cam_out[i].title +
          "</option>";
      }
      $("#select_camera_in").html(_html_cam_in);
      $("#select_camera_out").html(_html_cam_out);
      change_play_cam_gate_in($("#select_camera_in"), "videoElement_in");
      change_play_cam_gate_out($("#select_camera_out"), "videoElement_out");
    }).fail((err) => {
      console.log(err);
    });
  }
});
